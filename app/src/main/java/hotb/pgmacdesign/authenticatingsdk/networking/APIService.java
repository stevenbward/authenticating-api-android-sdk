package hotb.pgmacdesign.authenticatingsdk.networking;

import java.util.List;

import hotb.pgmacdesign.authenticatingsdk.datamodels.AvailableNetworks;
import hotb.pgmacdesign.authenticatingsdk.datamodels.CheckPhotoResults;
import hotb.pgmacdesign.authenticatingsdk.datamodels.CodeVerification;
import hotb.pgmacdesign.authenticatingsdk.datamodels.ConsentRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.PhoneRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleResponse;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SocialNetworkObj;
import hotb.pgmacdesign.authenticatingsdk.datamodels.TestResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.UploadPhotosObj;
import hotb.pgmacdesign.authenticatingsdk.datamodels.User;
import hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory.CriminalHistory;
import hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification.EducationStatusResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification.EducationVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification.EmploymentStatusResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification.EmploymentVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification.LicenseResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification.LicenseVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.quiz.QuizObject;
import hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz.VerifyQuizObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Link to web documentation: https://docs.authenticating.com
 * Created by pmacdowell on 2017-07-13.
 */

interface APIService {

    /**
     * @param authKey       API Key needed for calls
     * @param simpleRequest Access code is required.
     * @return {@link User}
     */
    @POST("user/summary")
    Call<User> getUser(@Header("Authorization") String authKey,
                       @Body SimpleRequest simpleRequest);

    /**
     * @param authKey API Key needed for calls
     * @param userObj Access code is required. Other fields will be updated if included.
     *                Fields that can be updated include: email, phone, firstName, lastName,
     *                address, city, state, zipCode, state, month, day, year, ssn
     * @return {@link User}
     */
    @PUT("user/update")
    Call<User> userUpdate(@Header("Authorization") String authKey,
                          @Body User userObj);

    /**
     * Send an SMS/ Text message to the phone number attached to the user accessCode being sent.
     *
     * @param authKey       API Key needed for calls
     * @param phoneRequest Require accessCode, phone is optional, if a phone is entered the current phone number will be updated
     *                     otherwise default is used
     * @return {@link SimpleResponse}
     */
    @POST("user/verifyPhone")
    Call<SimpleResponse> verifyPhone(@Header("Authorization") String authKey,
                                     @Body PhoneRequest phoneRequest);

    /**
     * Send an SMS/ Text message to the phone number attached to the user accessCode being sent.
     *
     * @param authKey   API Key needed for calls
     * @param codeVerification Only required fields include: accessCode, smsCode
     * @return {@link SimpleResponse}
     */
    @POST("user/verifyPhoneCode")
    Call<SimpleResponse> verifyPhonePinCode(@Header("Authorization") String authKey,
                                            @Body CodeVerification codeVerification);

    /**
     * Verify a User's email
     *
     * @param authKey API Key needed for calls
     * @return {@link SimpleResponse}
     */
    @POST("user/verifyEmail")
    Call<SimpleResponse> verifyEmail(@Header("Authorization") String authKey,
                                     @Body SimpleRequest simpleRequest);

    /**
     * Verify a User's email
     *
     * @param authKey API Key needed for calls
     * @return {@link SimpleResponse}
     */
    @POST("user/verifyEmailCode")
    Call<SimpleResponse> verifyEmailCode(@Header("Authorization") String authKey,
                                         @Body CodeVerification codeVerification);

    /**
     * Upload and compare 2 photos.
     *
     * @param authKey         API Key needed for calls
     * @param uploadPhotosObj Required fields here are: accessCode and both img1 / img2. Note that
     *                        both of the images are baseEncoded64 strings.
     * @return {@link SimpleResponse}
     */
    @POST("user/comparePhotos")
    Call<SimpleResponse> comparePhotos(@Header("Authorization") String authKey,
                                       @Body UploadPhotosObj uploadPhotosObj);

    /**
     * Upload a front and back of an ID for identity proof verification
     *
     * @param authKey         API Key needed for calls
     * @param uploadPhotosObj Required fields here are: accessCode and both idFront / idBack. Note that
     *                        both of the images are baseEncoded64 strings.
     * @return {@link SimpleResponse}
     */
    @POST("identity/document/scan")
    Call<SimpleResponse> uploadId(@Header("Authorization") String authKey,
                                  @Body UploadPhotosObj uploadPhotosObj);

    /**
     * Upload a front and back of an ID for identity proof verification (enhanced, see
     * {https://docs.authenticating.com} docs for details)
     *
     * @param authKey         API Key needed for calls
     * @param uploadPhotosObj Required fields here are: accessCode and both idFront / idBack. Note that
     *                        both of the images are baseEncoded64 strings.
     * @return {@link SimpleResponse}
     */
    @POST("identity/document/scan/enhanced")
    Call<SimpleResponse> uploadIdEnhanced(@Header("Authorization") String authKey,
                                          @Body UploadPhotosObj uploadPhotosObj);

    /**
     * Check the status of the uploadId endpoint background operation.
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest Required field is accessCode
     * @return {@link CheckPhotoResults}
     */
    @POST("identity/document/scan/status")
    Call<CheckPhotoResults> checkUploadId(@Header("Authorization") String authKey,
                                          @Body SimpleRequest simpleRequest);

    /**
     * Verify the uploadId endpoint background operation.
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest Required field is accessCode
     * @return {@link CheckPhotoResults}
     */
    @POST("identity/document/scan/data")
    Call<SimpleResponse> verifyUploadId(@Header("Authorization") String authKey,
                                        @Body SimpleRequest simpleRequest);

    /**
     * Upload a picture of a passport for the verification process. Note that only the front (The
     * portion with the quizData, usually on the first or second page) is required.
     *
     * @param authKey         API Key needed for calls
     * @param uploadPhotosObj Required fields here are: accessCode and idFront. Note that
     *                        the image is in a baseEncoded64 string format.
     * @return {@link SimpleResponse}
     */
    @POST("identity/passport/scan")
    Call<SimpleResponse> uploadPassport(@Header("Authorization") String authKey,
                                        @Body UploadPhotosObj uploadPhotosObj);

    /**
     * Check the status of the uploadPassport endpoint background operation.
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest Required field is accessCode
     * @return {@link CheckPhotoResults}
     */
    @POST("identity/passport/scan/status")
    Call<CheckPhotoResults> checkUploadPassport(@Header("Authorization") String authKey,
                                                @Body SimpleRequest simpleRequest);

    /**
     * Verify the uploadPassport endpoint background operation.
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest Required field is accessCode
     * @return {@link CheckPhotoResults}
     */
    @POST("identity/passport/scan/data")
    Call<SimpleResponse> verifyUploadPassport(@Header("Authorization") String authKey,
                                              @Body SimpleRequest simpleRequest);

    /**
     * Get the available networks for use in Social Networking test
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest Only required fields include: accessCode
     * @return {@link AvailableNetworks}
     */
    @POST("identity/getAvailableNetworks")
    Call<AvailableNetworks> getAvailableNetworks(@Header("Authorization") String authKey,
                                                 @Body SimpleRequest simpleRequest
    );

    /**
     * Used for passing the verification of social networks
     *
     * @param authKey          API Key needed for calls
     * @param socialNetworkObj Only required fields include: accessCode, network,
     *                         socialMediaAccessToken, socialMediaUserId
     * @return {@link SimpleResponse}
     */
    @POST("identity/verifySocialNetworks")
    Call<SimpleResponse> verifySocialNetworks(@Header("Authorization") String authKey,
                                              @Body SocialNetworkObj socialNetworkObj
    );

    /**
     * Get the quiz for the Identity Proof Test
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest only required field here is the accessCode
     * @return {@link QuizObject}
     */
    @POST("identity/kba")
    Call<QuizObject> getQuiz(@Header("Authorization") String authKey,
                             @Body SimpleRequest simpleRequest);

    /**
     * Verify the quiz results to complete the Identity test quiz
     *
     * @param authKey       API Key needed for calls
     * @param verifyQuizObj {@link hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz.QuestionAnswer}
     * @return {@link SimpleResponse}
     */
    @POST("identity/kba-verification ")
    Call<SimpleResponse> verifyQuiz(@Header("Authorization") String authKey,
                                    @Body VerifyQuizObject verifyQuizObj
    );

    /**
     * Background Proof Step, this generates a criminal background report.
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest only required value is accessCode
     * @return {@link SimpleResponse}
     */
    @POST("user/generateCriminalReport")
    Call<SimpleResponse> generateCriminalReport(@Header("Authorization") String authKey,
                                                @Body SimpleRequest simpleRequest);

//    ============ NEWLY ADDED ================

    @POST("user/create")
    Call<User> createUser(@Header("Authorization") String authKey, @Body User user);


    @POST("user/getTestResult")
    Call<TestResult> getTestResult(@Header("Authorization") String authKey,
                                   @Body SimpleRequest simpleRequest);

    /**
     * Triggers the start of employment verification
     *
     * @param authKey                       API Key needed for calls
     * @param employmentVerificationRequest all params required
     */
    @POST("employment/verify")
    Call<SimpleResponse> doEmploymentVerification(@Header("Authorization") String authKey,
                                                  @Body EmploymentVerificationRequest employmentVerificationRequest);

    /**
     * Get the employment verification result
     *
     * @param authKey       API Key needed for calls
     * @param simpleRequest access code required
     * @return {@link List<EmploymentStatusResult>}
     */
    @POST("employment/result")
    Call<List<EmploymentStatusResult>> getEmploymentVerification(@Header("Authorization") String authKey,
                                                                 @Body SimpleRequest simpleRequest);

    @POST("education/verify")
    Call<SimpleResponse> doEducationVerification(@Header("Authorization") String authKey,
                                                 @Body EducationVerificationRequest educationVerificationRequest);

    @POST("education/result")
    Call<List<EducationStatusResult>> getEducationVerification(@Header("Authorization") String authKey, @Body SimpleRequest simpleRequest);


    @POST("professional/license")
    Call<SimpleResponse> doLicenseVerification(@Header("Authorization") String authKey,
                                               @Body LicenseVerificationRequest licenseVerificationRequest);

    @POST("license/result")
    Call<List<LicenseResult>> getLicenseVerification(@Header("Authorization") String authKey,
                                                     @Body SimpleRequest simpleRequest);

    /**
     * @param authKey        API Key needed for calls
     * @param consentRequest Access code is required.
     * @return {@link User}
     */
    @POST("user/consent")
    Call<SimpleResponse> setConsent(@Header("Authorization") String authKey,
                                    @Body ConsentRequest consentRequest);

    /**
     * @param authKey        API Key needed for calls
     * @param simpleRequest Access code is required.
     * @return {@link User}
     */
    @POST("identity/request/criminal/report/seven")
    Call<CriminalHistory> get7YearCriminalHistory(@Header("Authorization") String authKey,
                                                  @Body SimpleRequest simpleRequest);

}
