package hotb.pgmacdesign.authenticatingsdk.networking;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;

import hotb.pgmacdesign.authenticatingsdk.datamodels.AuthenticatingException;
import hotb.pgmacdesign.authenticatingsdk.datamodels.ErrorParsingObj;

/**
 * Created by pmacdowell on 2017-07-25.
 */

class ErrorHandler {

    static void checkForAuthenticatingError(@Nullable String errorBodyString) throws AuthenticatingException {
        ErrorParsingObj errorParsingObj = parseMessageToError(errorBodyString);
        if (errorParsingObj != null && errorParsingObj.getErrorMessage() != null) {
            throw new AuthenticatingException(errorParsingObj.getErrorMessage(), errorParsingObj.getErrorMessage());
        }
        //If nothing else, means it is not an error
    }

    static void checkForAuthenticatingErrorObject(@NonNull Object responseBody) throws AuthenticatingException {
        String str = new Gson().toJson(responseBody);
        ErrorParsingObj errorParsingObj = parseMessageToError(str);
        if (errorParsingObj != null && errorParsingObj.getErrorMessage() != null) {
            throw new AuthenticatingException(errorParsingObj.getErrorMessage());
        }
        //If nothing else, means it is not an error
    }

    private static ErrorParsingObj parseMessageToError(String responseString){
        if(StringUtilities.isNullOrEmpty(responseString)){
            return null;
        }
        try {
            return (new Gson().fromJson(responseString, ErrorParsingObj.class));
        } catch (Exception e1) {
            return null;
        }
    }
}
