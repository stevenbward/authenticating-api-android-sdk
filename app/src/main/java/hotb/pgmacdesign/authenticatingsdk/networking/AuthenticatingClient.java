package hotb.pgmacdesign.authenticatingsdk.networking;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;


/**
 * The class which creates authenticating service
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class AuthenticatingClient {

    private static AuthenticatingClient authenticatingClient;
    private Interceptor customInterceptor = null;
    private boolean logging = false;

    private AuthenticatingClient() {
    }

    public static AuthenticatingClient getInstance() {
        if (authenticatingClient == null) {
            authenticatingClient = new AuthenticatingClient();
        }
        return authenticatingClient;
    }

    /**
     * @return whether logging is on or not
     */
    public boolean isLogging() {
        return logging;
    }

    /**
     * @param setLogging True means logging will take place, false will mean no logging
     */
    public void setLogging(boolean setLogging) {
        logging = setLogging;
    }

    public void setCustomInterceptor(Interceptor customInterceptor) {
        this.customInterceptor = customInterceptor;
    }

    public APIService getApiClient() {
        return getAuthenticatingRetrofitInstance().create(APIService.class);
    }

    public Retrofit getAuthenticatingRetrofitInstance() {

        //Next, create the OkHttpClient
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        //Configure SSL
        configureClient(builder);

        builder.addInterceptor(new Interceptor() {
            @NonNull
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request originalRequest = chain.request();
                String token = originalRequest.header("Authorization");
                Request.Builder bearerRequestBuilder = originalRequest.newBuilder();
                bearerRequestBuilder.removeHeader("Authorization");
                if (token != null)
                    bearerRequestBuilder.addHeader("Authorization", "Bearer " + token);
                bearerRequestBuilder.method(originalRequest.method(), originalRequest.body());

                return chain.proceed(bearerRequestBuilder.build());
            }
        });
        builder.addInterceptor(new LoggingInterceptor());
        if (customInterceptor != null)
            builder.addInterceptor(customInterceptor);

        //Build the client
        OkHttpClient client = builder.build();

        //Create the retrofit object, which will use the variables/ objects we have created above
        String BASE_URL = "https://api-v3.authenticating.com/";
        Retrofit.Builder myBuilder = new Retrofit.Builder()
                .baseUrl(BASE_URL/*AuthenticatingConstants.STAGING_URL*/)
                .addConverterFactory(new CustomConverterFactory());

        myBuilder.client(client);

        return myBuilder.build();
    }

    /**
     * This class will configure the OkHttpClient to add things like SSL, certs, etc.
     *
     * @param builder The builder that will be altered and returned
     *                For more information on this, please see
     *                {@link okhttp3.OkHttpClient.Builder} <-- sslSocketFactory
     */
    private void configureClient(final OkHttpClient.Builder builder) {

        try {
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:"
                        + Arrays.toString(trustManagers));
            }
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            builder.sslSocketFactory(sslSocketFactory, trustManager);

        } catch (KeyManagementException kme) {
            kme.printStackTrace();
        } catch (NoSuchAlgorithmException nsa) {
            nsa.printStackTrace();
        } catch (KeyStoreException kse) {
            kse.printStackTrace();
        } catch (IllegalStateException ise) {
            ise.printStackTrace();
        }
    }
}



