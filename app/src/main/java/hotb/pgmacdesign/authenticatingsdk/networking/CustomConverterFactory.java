package hotb.pgmacdesign.authenticatingsdk.networking;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by pmacdowell on 11/7/2016.
 */
class CustomConverterFactory extends Converter.Factory {

    private static final Type TYPE_BOOLEAN = Boolean.TYPE;
    private static final Type TYPE_DOUBLE = Double.TYPE;
    private static final Type TYPE_INTEGER = Integer.TYPE;
    private static final Type TYPE_STRING = new TypeToken<String>(){}.getType();

    CustomConverterFactory() {
        super();
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        if(type == TYPE_BOOLEAN){
            //Boolean
            try {
                return new Converter<ResponseBody, Boolean>() {
                    @Override
                    public Boolean convert(@NonNull ResponseBody value) throws IOException {
                        return Boolean.parseBoolean(value.string());
                    }
                };
            } catch (Exception e){
                e.printStackTrace();
            }

        } else if(type == TYPE_DOUBLE){
            //Double
            try {
                return new Converter<ResponseBody, Double>() {
                    @Override
                    public Double convert(@NonNull ResponseBody value) throws IOException {
                        return Double.parseDouble(value.string());
                    }
                };
            } catch (Exception e){
                e.printStackTrace();
            }

        } else if(type == TYPE_INTEGER){
            //Integer
            try {
                return new Converter<ResponseBody, Integer>() {
                    @Override
                    public Integer convert(@NonNull ResponseBody value) throws IOException {
                        return Integer.parseInt(value.string());
                    }
                };
            } catch (Exception e){
                e.printStackTrace();
            }

        } else if(type == (TYPE_STRING)){
            //String
            try {
                return new Converter<ResponseBody, String>() {
                    @Override
                    public String convert(@NonNull ResponseBody value) throws IOException {
                        return value.string();
                    }
                };
            } catch (Exception e){
                e.printStackTrace();
            }

        } else {
            try {
                return GsonConverterFactory
                        .create(new GsonBuilder().setLenient().serializeNulls().create()).responseBodyConverter(type, annotations, retrofit);
            } catch (Exception e){
                Log.d("CustomConverterFactory",
                        "Make sure you don't have the same '@Serialized' string name declaration over 2 different variables. This will cause an exception. See this link for details: https://stackoverflow.com/a/42517143/2480714");
                e.printStackTrace();
            }
        }

        //If a catch gets hit
        return super.responseBodyConverter(type, annotations, retrofit);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type,
                                                          Annotation[] parameterAnnotations,
                                                          Annotation[] methodAnnotations,
                                                          Retrofit retrofit) {
        return GsonConverterFactory
                .create().requestBodyConverter(type, parameterAnnotations,
                        methodAnnotations, retrofit);
    }

    @Override
    public Converter<?, String> stringConverter(Type type, Annotation[] annotations,
                                                Retrofit retrofit) {
        return super.stringConverter(type, annotations, retrofit);
    }
}
