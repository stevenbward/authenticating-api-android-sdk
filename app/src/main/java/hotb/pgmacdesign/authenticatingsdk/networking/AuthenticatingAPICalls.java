package hotb.pgmacdesign.authenticatingsdk.networking;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import hotb.pgmacdesign.authenticatingsdk.datamodels.AuthenticatingException;
import hotb.pgmacdesign.authenticatingsdk.datamodels.AvailableNetworks;
import hotb.pgmacdesign.authenticatingsdk.datamodels.CheckPhotoResults;
import hotb.pgmacdesign.authenticatingsdk.datamodels.CodeVerification;
import hotb.pgmacdesign.authenticatingsdk.datamodels.ConsentRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.PhoneRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleResponse;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SocialNetworkObj;
import hotb.pgmacdesign.authenticatingsdk.datamodels.SupportedRegions;
import hotb.pgmacdesign.authenticatingsdk.datamodels.TestResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.UploadPhotosObj;
import hotb.pgmacdesign.authenticatingsdk.datamodels.User;
import hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory.CriminalHistory;
import hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification.Education;
import hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification.EducationStatusResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification.EducationVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification.EmploymentDetail;
import hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification.EmploymentStatusResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification.EmploymentVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification.License;
import hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification.LicenseResult;
import hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification.LicenseVerificationRequest;
import hotb.pgmacdesign.authenticatingsdk.datamodels.quiz.QuizObject;
import hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz.QuestionAnswer;
import hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz.VerifyQuizObject;
import hotb.pgmacdesign.authenticatingsdk.interfaces.AuthenticatingListener;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static hotb.pgmacdesign.authenticatingsdk.networking.StringUtilities.isNullOrEmpty;
import static hotb.pgmacdesign.authenticatingsdk.networking.StringUtilities.keepNumbersOnly;

/**
 * Class that houses API calls. These can be used or not if you want to define your own web
 * calls using the interface {@link APIService} obtained from getAPIClient() in {@link AuthenticatingClient}
 * Link to web documentation: https://docs.authenticating.com
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public class AuthenticatingAPICalls {

    private static final String NO_INTERNET = "No network connection, please check your internet connectivity and try again";
    private static final String UNAUTHORIZED_REQUEST = "Unauthorized request";
    private static final String GENERIC_ERROR_STRING = "An unknown error has occurred";
    private static final String SENT_IMAGE_BAD = "One or more of the passed image URIs was either null or was unable to be parsed";
    private static final String MUST_INCLUDE_ACCESS_CODE = "You must include the AccessCode in this call";
    private static final String MISSING_AUTH_KEY = "You did not include your authKey. This is obtained when you register for an account. Calls will not function without this key";
    private static final String PARSING_CONVERSION_ERROR = "Could not convert server response. Please enabling logging to see full request and response logs.";

    private static APIService myService;
    private static AsyncTask<Void, Void, Void> uploadPhotosAsynctask;

    static {
        myService = AuthenticatingClient.getInstance().getApiClient();
    }

    private static <T> T doSynchronousTask(Call<T> call) throws AuthenticatingException {
        try {
            Response<T> response = call.execute();

            T t = response.body();
            ResponseBody errorBody = response.errorBody();
            if (t != null) ErrorHandler.checkForAuthenticatingErrorObject(t);

            String errorBodyString = null;
            if (errorBody != null) errorBodyString = errorBody.string();
            ErrorHandler.checkForAuthenticatingError(errorBodyString);

            return t;
        } catch (Exception ioe) {
            throw new AuthenticatingException(ioe);
        }
    }

    public static <T> void doAsynchronousTask(final AuthenticatingListener<T> listener,
                                              Call<T> call) {

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                try {
                    T t = response.body();
                    ResponseBody errorBody = response.errorBody();

                    String errorBodyString = null;
                    if (errorBody != null) errorBodyString = errorBody.string();
                    ErrorHandler.checkForAuthenticatingError(errorBodyString);

                    if (t != null) {
                        ErrorHandler.checkForAuthenticatingErrorObject(t);
                        listener.onSuccess(t);
                    } else listener.onError(new AuthenticatingException(PARSING_CONVERSION_ERROR));

                } catch (AuthenticatingException authEx) {
                    listener.onError(authEx);
                } catch (Exception e) {
                    listener.onError(new AuthenticatingException(e));
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                listener.onError(new AuthenticatingException(t));
            }
        });
    }

    private static <T> T doAction(Call<T> call, String accessCode) throws AuthenticatingException {
        if (accessCode != null && accessCode.isEmpty()) {
            throw new AuthenticatingException(MISSING_AUTH_KEY);
        }
        return doSynchronousTask(call);
    }

    public static <T> AuthenticatingNetworkCall doAction(Call<T> call, String accessCode, @NonNull AuthenticatingListener<T> listener) {
        if (accessCode != null && accessCode.isEmpty()) {
            listener.onError(new AuthenticatingException(MISSING_AUTH_KEY));
        }

        doAsynchronousTask(listener, call);
        return new AuthenticatingNetworkCall(call);
    }

    private static Integer getCountryCode(SupportedRegions supportedRegions) {
        Integer region = null;
        if (supportedRegions != null) {
            switch (supportedRegions) {
                case UNITED_STATES:
                    region = 0;
                    break;
                case AUSTRALIA:
                    region = 4;
                    break;
                case ASIA:
                    region = 5;
                    break;
                case CANADA:
                    region = 1;
                    break;
                case AMERICA:
                    region = 2;
                    break;
                case EUROPE:
                    region = 3;
                    break;
                case AFRICA:
                    region = 7;
                    break;
                case GENERAL_DOCUMENTS:
                    region = 6;
                    break;
            }
        }
        return region;
    }

    /**
     * For requesting employment verification
     * Synchronous call, should be executed in a background thread
     *
     * @param companyAPIKey        company api key
     * @param accessCode           user access code
     * @param employmentDetailList {@link List<EmploymentDetail>} list of employment details
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException an exception when error happens
     */
    public static SimpleResponse doEmploymentVerification(@NonNull String companyAPIKey,
                                                          @NonNull String accessCode,
                                                          @NonNull List<EmploymentDetail> employmentDetailList) throws AuthenticatingException {
        return doAction(myService.doEmploymentVerification(companyAPIKey, new EmploymentVerificationRequest(accessCode, employmentDetailList)), accessCode);
    }

    /**
     * For requesting employment verification.
     * Asynchronous
     *
     * @param listener             {@link AuthenticatingListener <SimpleResponse>}
     * @param companyAPIKey        company api key
     * @param accessCode           user access code
     * @param employmentDetailList {@link List<EmploymentDetail>}
     */
    public static AuthenticatingNetworkCall doEmploymentVerificationAsync(@NonNull final AuthenticatingListener<SimpleResponse> listener,
                                                                          @NonNull String companyAPIKey,
                                                                          @NonNull String accessCode,
                                                                          @NonNull List<EmploymentDetail> employmentDetailList) {
        return doAction(myService.doEmploymentVerification(companyAPIKey, new EmploymentVerificationRequest(accessCode, employmentDetailList)), accessCode, listener);
    }

    /**
     * For requesting status on employment verification.
     * Synchronous call, should be executed in a background thread
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @return {@link List<EmploymentStatusResult>} list of employment status result
     * @throws AuthenticatingException when an error happens
     */
    public static List<EmploymentStatusResult> getEmploymentVerificationResult(@NonNull String companyAPIKey,
                                                                               @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getEmploymentVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode);

    }

    /**
     * For requesting status on employment verification.
     * Asynchronous
     *
     * @param listener      {@link AuthenticatingListener <List>} with list of {@link EmploymentStatusResult}
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     */
    public static AuthenticatingNetworkCall getEmploymentVerificationResultAsync(@NonNull String companyAPIKey,
                                                                                 @NonNull String accessCode,
                                                                                 @NonNull final AuthenticatingListener<List<EmploymentStatusResult>> listener) {
        return doAction(myService.getEmploymentVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * For requesting education verification.
     * Synchronous call
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @param educationList list of education to verify
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException will be thrown on error
     */
    public static SimpleResponse doEducationVerification(@NonNull String companyAPIKey,
                                                         @NonNull String accessCode,
                                                         @NonNull List<Education> educationList) throws AuthenticatingException {
        return doAction(myService.doEducationVerification(companyAPIKey, new EducationVerificationRequest(accessCode, educationList)), accessCode);

    }

    /**
     * For requesting education verification.
     * Asynchronous call
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @param educationList list of education to verify
     * @param listener      {@link AuthenticatingListener <SimpleResponse>}
     */
    public static AuthenticatingNetworkCall doEducationVerificationAsync(@NonNull String companyAPIKey,
                                                                         @NonNull String accessCode,
                                                                         @NonNull List<Education> educationList,
                                                                         @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.doEducationVerification(companyAPIKey, new EducationVerificationRequest(accessCode, educationList)), accessCode, listener);
    }

    /**
     * For requesting education verification result.
     * Synchronous call
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @return List<EducationStatusResult>
     * @throws AuthenticatingException will be thrown if an error occurs
     */
    public static List<EducationStatusResult> getEducationVerificationResult(@NonNull String companyAPIKey,
                                                                             @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getEducationVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode);

    }

    /**
     * For requesting education verification result.
     * Asynchronous call
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @param listener      {@link AuthenticatingListener <List>} with list of {@link EducationStatusResult}
     */
    public static AuthenticatingNetworkCall getEducationVerificationResultAsync(@NonNull String companyAPIKey,
                                                                                @NonNull String accessCode,
                                                                                @NonNull AuthenticatingListener<List<EducationStatusResult>> listener) {
        return doAction(myService.getEducationVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * For requesting education verification result.
     *
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @param licenseList   the list of license to verify
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException will be thrown when an error happens
     */
    public static SimpleResponse doLicenseVerification(@NonNull String companyAPIKey,
                                                       @NonNull String accessCode,
                                                       @NonNull List<License> licenseList) throws AuthenticatingException {
        return doAction(myService.doLicenseVerification(companyAPIKey, new LicenseVerificationRequest(accessCode, licenseList)), accessCode);

    }

    /**
     * @param companyAPIKey company api key
     * @param accessCode    user access code
     * @param licenseList   the list of license to verify
     * @param listener      {@link AuthenticatingListener <SimpleResponse>}
     */
    public static AuthenticatingNetworkCall doLicenseVerificationAsync(@NonNull String companyAPIKey,
                                                                       @NonNull String accessCode,
                                                                       @NonNull List<License> licenseList,
                                                                       @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.doLicenseVerification(companyAPIKey, new LicenseVerificationRequest(accessCode, licenseList)), accessCode, listener);
    }

    /**
     * @param companyAPIKey the company api key
     * @param accessCode    user access code
     * @return a list of license result
     * @throws AuthenticatingException will be thrown when an errorhappens
     */
    public static List<LicenseResult> getLicenseVerificationResult(@NonNull String companyAPIKey,
                                                                   @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getLicenseVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode);

    }

    /**
     * @param companyAPIKey the company api key
     * @param accessCode    user access code
     * @param listener      {@link AuthenticatingListener <List>} with list of {@link LicenseResult}
     */
    public static AuthenticatingNetworkCall getLicenseVerificationResultAsync(@NonNull String companyAPIKey,
                                                                              @NonNull String accessCode,
                                                                              @NonNull AuthenticatingListener<List<LicenseResult>> listener) {
        return doAction(myService.getLicenseVerification(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Get the user
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link User}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static User getUser(@NonNull String companyAPIKey,
                               @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getUser(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }


//    =================UPDATED V2================

    /**
     * Get the user object
     *
     * @param listener      {@link AuthenticatingListener <User>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall getUserAsync(@NonNull String companyAPIKey,
                                                         @NonNull String accessCode,
                                                         @NonNull final AuthenticatingListener<User> listener) {
        return doAction(myService.getUser(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Update User (Overloaded to allow a user object to be passed in)
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param user          {@link User} IMPORTANT: Must pass accessCode in User object
     * @return {@link User}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static User updateUser(@NonNull String companyAPIKey, @NonNull User user) throws AuthenticatingException {
        return doAction(myService.userUpdate(companyAPIKey, user), user.getUserAccessCode());
    }

    /**
     * Update user (overloaded for User Object)
     *
     * @param listener      {@link AuthenticatingListener<User>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param user          {@link User} IMPORTANT: Must pass accessCode in User object
     */
    public static AuthenticatingNetworkCall updateUserAsync(@NonNull String companyAPIKey,
                                                            @NonNull User user,
                                                            @NonNull AuthenticatingListener<User> listener) {
        return doAction(myService.userUpdate(companyAPIKey, user), user.getUserAccessCode(), listener);
    }

    /**
     * Update a user
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param firstName     First name of user (IE: John)
     * @param lastName      Last name of User (IE: Smith)
     * @param address       Address of User (IE: 123 Fake St)
     * @param city          City of the User (IE: Los Angeles)
     * @param state         State Abbreviation of User (IE: CA or NY)
     * @param zipcode       5 digit zip code / postal code of user (IE: 90210 or 20500)
     * @param dob           Date of Birth in dd-MM-yyyy format
     * @param email         Email (IE, email@email.com)
     * @param phoneNumber   Phone number, numbers only (IE: 2138675309)
     * @param ssn           Social Security Number, 9 digits (IE: 123456789)
     * @return {@link User}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static Object updateUser(@NonNull String companyAPIKey,
                                    @NonNull String accessCode,
                                    @Nullable String firstName,
                                    @Nullable String middleName,
                                    @Nullable String lastName,
                                    @Nullable String address,
                                    @Nullable String city,
                                    @Nullable String state,
                                    @Nullable String zipcode,
                                    @Nullable String dob,
                                    @Nullable String email,
                                    @Nullable String phoneNumber,
                                    @Nullable String ssn,
                                    @Nullable Integer houseNumber,
                                    @Nullable String streetName) throws AuthenticatingException {

        User user = new User();
        user.setUserAccessCode(accessCode);
        if (!isNullOrEmpty(firstName))
            user.setFirstName(firstName);
        if (!isNullOrEmpty(middleName))
            user.setMiddleName(middleName);
        if (!isNullOrEmpty(lastName))
            user.setLastName(lastName);
        if (!isNullOrEmpty(dob))
            user.setDateOfBirth(dob);
        if (!isNullOrEmpty(address))
            user.setAddress(address);
        if (!isNullOrEmpty(city))
            user.setCity(city);
        if (!isNullOrEmpty(state))
            user.setState(state);
        if (!isNullOrEmpty(zipcode))
            user.setZipcode(zipcode);
        if (!isNullOrEmpty(ssn))
            user.setSsn(ssn);
        if (!isNullOrEmpty(email))
            user.setEmail(email);
        if (!isNullOrEmpty(keepNumbersOnly(phoneNumber)))
            user.setPhone(keepNumbersOnly(phoneNumber));
        if(!isNullOrEmpty(houseNumber))
            user.setHouseNumber(houseNumber);
        if(!isNullOrEmpty(streetName))
            user.setStreetName(streetName);
        return updateUser(companyAPIKey, user);
    }

    /**
     * Update the user object. Other than the usual accessCode, apikey, and clientId, other fields
     * are optional
     *
     * @param listener      {@link AuthenticatingListener<User>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param firstName     First name of user (IE: John)
     * @param lastName      Last name of User (IE: Smith)
     * @param address       Address of User (IE: 123 Fake St)
     * @param city          City of the User (IE: Los Angeles)
     * @param state         State Abbreviation of User (IE: CA or NY)
     * @param zipcode       5 digit zip code / postal code of user (IE: 90210 or 20500)
     * @param dob           Date of Birth in dd-MM-yyyy format
     * @param email         Email (IE, email@email.com)
     * @param phoneNumber   Phone number, numbers only (IE: 2138675309)
     * @param ssn           Social Security Number, 9 digits (IE: 123456789)
     */
    public static AuthenticatingNetworkCall updateUserAsync(@NonNull String companyAPIKey,
                                                            @NonNull String accessCode,
                                                            @Nullable String firstName,
                                                            @Nullable String middleName,
                                                            @Nullable String lastName,
                                                            @Nullable String address,
                                                            @Nullable String city,
                                                            @Nullable String state,
                                                            @Nullable String zipcode,
                                                            @Nullable String dob,
                                                            @Nullable String email,
                                                            @Nullable String phoneNumber,
                                                            @Nullable String ssn,
                                                            @Nullable Integer houseNumber,
                                                            @Nullable String streetName,
                                                            @NonNull AuthenticatingListener<User> listener) {

        User user = new User();
        user.setUserAccessCode(accessCode);
        if (!isNullOrEmpty(firstName))
            user.setFirstName(firstName);
        if (!isNullOrEmpty(middleName))
            user.setMiddleName(middleName);
        if (!isNullOrEmpty(lastName))
            user.setLastName(lastName);
        if (!isNullOrEmpty(dob))
            try {
                user.setDateOfBirth(dob);
            } catch (AuthenticatingException ex) {
                listener.onError(ex);
                return null;
            }
        if (!isNullOrEmpty(address))
            user.setAddress(address);
        if (!isNullOrEmpty(city))
            user.setCity(city);
        if (!isNullOrEmpty(state))
            user.setState(state);
        if (!isNullOrEmpty(zipcode))
            user.setZipcode(zipcode);
        if (!isNullOrEmpty(ssn))
            user.setSsn(ssn);
        if (!isNullOrEmpty(email))
            user.setEmail(email);
        if (!isNullOrEmpty(keepNumbersOnly(phoneNumber)))
            user.setPhone(keepNumbersOnly(phoneNumber));
        if(!isNullOrEmpty(houseNumber))
            user.setHouseNumber(houseNumber);
        if(!isNullOrEmpty(streetName))
            user.setStreetName(streetName);
        return updateUserAsync(companyAPIKey, user, listener);
    }

    /**
     * This endpoint will send a text / SMS to the user with a code for them to enter in the verifyCode
     * endpoint.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param phone         The phone
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyPhone(@NonNull String companyAPIKey,
                                             @NonNull String phone,
                                             @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.verifyPhone(companyAPIKey, new PhoneRequest(accessCode, phone)), accessCode);
    }

    /**
     * This endpoint will send a text / SMS to the user with a code for them to enter in
     * endpoint.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall verifyPhoneAsync(@NonNull String companyAPIKey,
                                                             @NonNull String phone,
                                                             @NonNull String accessCode,
                                                             @NonNull final AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyPhone(companyAPIKey, new PhoneRequest(accessCode, phone)), accessCode, listener);
    }

    /**
     * Used after receiving an SMS and including it here as the code received to finish
     * the test and pass the SMS portion.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param smsCode       The code received in the user's SMS to be sent outbound.
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyPhoneCode(String companyAPIKey,
                                                 String accessCode, String smsCode) throws AuthenticatingException {

        return doAction(myService.verifyPhonePinCode(companyAPIKey, new CodeVerification(accessCode, smsCode)), accessCode);
    }

    /**
     * Used after receiving an SMS and including it here as the code received to finish
     * the test and pass the SMS portion.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param smsCode       The code received in the user's SMS to be sent outbound.
     */
    public static AuthenticatingNetworkCall verifyPhoneCodeAsync(@NonNull String companyAPIKey,
                                                                 @NonNull String accessCode, String smsCode,
                                                                 @NonNull final AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyPhonePinCode(companyAPIKey, new CodeVerification(accessCode, smsCode)), accessCode, listener);
    }

    /**
     * Verify an email
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyEmail(String companyAPIKey,
                                             String accessCode) throws AuthenticatingException {
        return doAction(myService.verifyEmail(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Verify a user's email
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall verifyEmailAsync(@NonNull String companyAPIKey,
                                                             @NonNull String accessCode,
                                                             @NonNull final AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyEmail(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Verify an email
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyEmailCode(@NonNull String companyAPIKey,
                                                 @NonNull String accessCode,
                                                 @NonNull String emailCode) throws AuthenticatingException {
        return doAction(myService.verifyEmailCode(companyAPIKey, new CodeVerification(accessCode, emailCode)), accessCode);
    }

    /**
     * Verify a user's email
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall verifyEmailCodeAsync(@NonNull String companyAPIKey,
                                                                 @NonNull String accessCode,
                                                                 @NonNull String emailCode,
                                                                 @NonNull final AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyEmailCode(companyAPIKey, new CodeVerification(accessCode, emailCode)), accessCode, listener);
    }

    /**
     * Upload 2 photos to the endpoint for Photo proof.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param photo1Bitmap  First Photo File to parse.
     * @param photo2Bitmap  Second Photo File to parse.
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse comparePhotos(@NonNull String companyAPIKey, @NonNull String accessCode,
                                               @NonNull Bitmap photo1Bitmap, @NonNull Bitmap photo2Bitmap) throws AuthenticatingException {

        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, photo1Bitmap,
                photo2Bitmap, UploadIdTypes.comparePhotos, null);
    }

    /**
     * Upload 2 photos to the endpoint for uploadId and identify verification.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     * @param idBackBitmap  Second Photo File to parse.
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse uploadId(@NonNull String companyAPIKey, @NonNull String accessCode,
                                          @NonNull Bitmap idFrontBitmap, @NonNull Bitmap idBackBitmap,
                                          @NonNull SupportedRegions supportedRegions) throws AuthenticatingException {

        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                idBackBitmap, UploadIdTypes.uploadId, supportedRegions);
    }

    /**
     * Upload a picture of a passport for the verification process. Note that only the front (The
     * portion with the quizData, usually on the first or second page) is required.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse uploadPassport(String companyAPIKey, String accessCode,
                                                Bitmap idFrontBitmap) throws AuthenticatingException {

        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                null, UploadIdTypes.uploadPassport, null);
    }

    /**
     * Upload 2 photos to the endpoint for uploadIdEnhanced and identify verification.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     * @param idBackBitmap  Second Photo File to parse.
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse uploadIdEnhanced(@NonNull String companyAPIKey, @NonNull String accessCode,
                                                  @NonNull Bitmap idFrontBitmap, @NonNull Bitmap idBackBitmap,
                                                  @NonNull SupportedRegions supportedRegions) throws AuthenticatingException {

        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                idBackBitmap, UploadIdTypes.uploadIdEnhanced, supportedRegions);
    }

    private static SimpleResponse uploadIdEndpointsJoiner(String companyAPIKey,
                                                          String accessCode,
                                                          Bitmap idFrontBitmap,
                                                          Bitmap idBackBitmap,
                                                          UploadIdTypes type,
                                                          SupportedRegions supportedRegions) throws AuthenticatingException {

        if (type == UploadIdTypes.uploadPassport) {
            if (idFrontBitmap == null)
                throw new AuthenticatingException("Please pass in a valid front bitmap");
        } else if (idFrontBitmap == null || idBackBitmap == null)
            throw new AuthenticatingException("Please pass in valid bitmaps");

        if (type == UploadIdTypes.uploadPassport)
            if (idFrontBitmap.getRowBytes() <= 0 || idFrontBitmap.getHeight() <= 0)
                throw new AuthenticatingException("Please pass in a valid front bitmap");
            else if (idFrontBitmap.getRowBytes() <= 0 || idFrontBitmap.getHeight() <= 0 ||
                    idBackBitmap.getRowBytes() <= 0 || idBackBitmap.getHeight() <= 0)
                throw new AuthenticatingException("Please pass in valid bitmaps");

        if (type == UploadIdTypes.uploadPassport) {
            if (ConvertPhotoAsync.isBitmapTooLarge(idFrontBitmap))
                idFrontBitmap = ConvertPhotoAsync.resizePhoto(idFrontBitmap);
        } else {
            if (ConvertPhotoAsync.isBitmapTooLarge(idFrontBitmap))
                idFrontBitmap = ConvertPhotoAsync.resizePhoto(idFrontBitmap);
            if (ConvertPhotoAsync.isBitmapTooLarge(idBackBitmap))
                idBackBitmap = ConvertPhotoAsync.resizePhoto(idBackBitmap);
        }

        UploadPhotosObj uploadPhotosObj = new UploadPhotosObj();
        uploadPhotosObj.setUserAccessCode(accessCode);
        String idFront, idBack = null;
        try {
            if (type == UploadIdTypes.uploadPassport) {
                idFront = ConvertPhotoAsync.encodeImage(idFrontBitmap);
            } else {
                idFront = ConvertPhotoAsync.encodeImage(idFrontBitmap);
                idBack = ConvertPhotoAsync.encodeImage(idBackBitmap);
            }
        } catch (Exception e) {
            return null;
        }

        uploadPhotosObj.setCountry(getCountryCode(supportedRegions));

        Call<SimpleResponse> call;

        switch (type) {
            case uploadPassport:
                uploadPhotosObj.setIdFront(idFront);
                call = myService.uploadPassport(companyAPIKey, uploadPhotosObj);
                break;

            case uploadId:
                uploadPhotosObj.setIdFront(idFront);
                uploadPhotosObj.setIdBack(idBack);
                call = myService.uploadId(companyAPIKey, uploadPhotosObj);

                break;
            case uploadIdEnhanced:
                uploadPhotosObj.setIdFront(idFront);
                uploadPhotosObj.setIdBack(idBack);
                call = myService.uploadIdEnhanced(companyAPIKey, uploadPhotosObj);
                break;

            case comparePhotos:
            default:
                uploadPhotosObj.setImg1(idFront);
                uploadPhotosObj.setImg2(idBack);
                call = myService.comparePhotos(companyAPIKey, uploadPhotosObj);
                break;
        }

        return doAction(call, accessCode);
    }

    /**
     * Upload 2 photos to the endpoint for Photo proof. This method will run all bitmap conversion
     * and base64 string encoding on a thread and not impact the main UI thread.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param photo1Bitmap  First Photo File to parse.
     * @param photo2Bitmap  Second Photo File to parse.
     */
    public static AuthenticatingNetworkCall comparePhotosAsync(@NonNull String companyAPIKey,
                                                               @NonNull String accessCode,
                                                               @NonNull Bitmap photo1Bitmap,
                                                               @NonNull Bitmap photo2Bitmap,
                                                               @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return AuthenticatingAPICalls.uploadIdEndpointsJoiner(companyAPIKey, accessCode,
                photo1Bitmap, photo2Bitmap, UploadIdTypes.comparePhotos, null, listener);
    }

    /**
     * Upload 2 photos to the endpoint for uploadIdEnhanced and identify verification.
     * This method will run all bitmap conversion and base64 string encoding on a thread and
     * not impact the main UI thread.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     * @param idBackBitmap  Second Photo File to parse.
     */
    public static AuthenticatingNetworkCall uploadIdEnhancedAsync(@NonNull String companyAPIKey,
                                                                  @NonNull String accessCode,
                                                                  @NonNull Bitmap idFrontBitmap,
                                                                  @NonNull Bitmap idBackBitmap,
                                                                  @NonNull SupportedRegions supportedRegions,
                                                                  AuthenticatingListener<SimpleResponse> listener) {
        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                idBackBitmap, UploadIdTypes.uploadIdEnhanced, supportedRegions, listener);
    }

    /**
     * Upload 2 photos to the endpoint for uploadId and identify verification. This method will run all bitmap conversion
     * and base64 string encoding on a thread and not impact the main UI thread.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     * @param idBackBitmap  Second Photo File to parse.
     */
    public static AuthenticatingNetworkCall uploadIdAsync(@NonNull String companyAPIKey,
                                                          @NonNull String accessCode,
                                                          @NonNull Bitmap idFrontBitmap,
                                                          @NonNull Bitmap idBackBitmap,
                                                          @NonNull SupportedRegions supportedRegions,
                                                          @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                idBackBitmap, UploadIdTypes.uploadId, supportedRegions, listener);
    }

    /**
     * Upload a picture of a passport for the verification process. Note that only the front (The
     * portion with the quizData, usually on the first or second page) is required. This method will run all bitmap conversion
     * and base64 string encoding on a thread and not impact the main UI thread.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param idFrontBitmap First Photo File to parse.
     */
    public static AuthenticatingNetworkCall uploadPassportAsync(@NonNull String companyAPIKey,
                                                                @NonNull String accessCode,
                                                                @NonNull Bitmap idFrontBitmap,
                                                                @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return uploadIdEndpointsJoiner(companyAPIKey, accessCode, idFrontBitmap,
                null, UploadIdTypes.uploadPassport, null, listener);
    }

    private static AuthenticatingNetworkCall uploadIdEndpointsJoiner(@NonNull final String companyAPIKey,
                                                                     @NonNull final String accessCode,
                                                                     @NonNull Bitmap idFrontBitmap,
                                                                     @Nullable Bitmap idBackBitmap,
                                                                     @NonNull final UploadIdTypes type,
                                                                     @Nullable final SupportedRegions supportedRegions,
                                                                     @NonNull final AuthenticatingListener<SimpleResponse> listener) {

        if (type != UploadIdTypes.uploadPassport) {
            if (idBackBitmap == null) {
                listener.onError(new AuthenticatingException("Please pass in a valid photo"));
                return null;
            }
        }

        if (type == UploadIdTypes.uploadPassport) {
            if (idFrontBitmap.getRowBytes() <= 0 || idFrontBitmap.getHeight() <= 0) {
                listener.onError(new AuthenticatingException("Please pass in a valid photo"));
                return null;
            }
        } else {
            if (idFrontBitmap.getRowBytes() <= 0 || idFrontBitmap.getHeight() <= 0 ||
                    idBackBitmap.getRowBytes() <= 0 || idBackBitmap.getHeight() <= 0) {
                listener.onError(new AuthenticatingException("Please pass in a valid photo"));
                return null;
            }
        }

        final AuthenticatingNetworkCall authenticatingNetworkCall = new AuthenticatingNetworkCall();


        final AuthenticatingListener<UploadPhotosObj> authenticatingListener = new AuthenticatingListener<UploadPhotosObj>() {
            @Override
            public void onSuccess(@NonNull UploadPhotosObj uploadPhotosObj) {
                uploadPhotosObj.setUserAccessCode(accessCode);
                uploadPhotosObj.setCountry(getCountryCode(supportedRegions));
                Call<SimpleResponse> call;
                switch (type) {
                    case uploadPassport:
                        call = myService.uploadPassport(companyAPIKey, uploadPhotosObj);
                        break;

                    case uploadId:
                        call = myService.uploadId(companyAPIKey, uploadPhotosObj);
                        break;

                    case uploadIdEnhanced:
                        call = myService.uploadIdEnhanced(companyAPIKey, uploadPhotosObj);
                        break;

                    case comparePhotos:
                    default:
                        call = myService.comparePhotos(companyAPIKey, uploadPhotosObj);
                        break;
                }
                authenticatingNetworkCall.setCall(call);
                doAction(call, accessCode, listener);
            }

            @Override
            public void onError(@NonNull AuthenticatingException authenticatingException) {
                listener.onError(authenticatingException);
            }
        };

        new ConvertPhotoAsync(idFrontBitmap, idBackBitmap, type, authenticatingListener).execute();

        return authenticatingNetworkCall;
    }

    /**
     * Check the current status of the asynchronous image processing on the server
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link CheckPhotoResults}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static CheckPhotoResults checkUploadId(@NonNull String companyAPIKey,
                                                  @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.checkUploadId(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Verify the current status of the asynchronous image processing on the server
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link CheckPhotoResults}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static CheckPhotoResults verifyUploadId(@NonNull String companyAPIKey,
                                                   @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.checkUploadId(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Check the current status of the asynchronous image processing on the server
     *
     * @param listener      {@link AuthenticatingListener <CheckPhotoResults>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall checkUploadIdAsync(@NonNull String companyAPIKey,
                                                               @NonNull String accessCode,
                                                               @NonNull AuthenticatingListener<CheckPhotoResults> listener) {
        return doAction(myService.checkUploadId(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Verify the current status of the asynchronous image processing on the server
     *
     * @param listener      {@link AuthenticatingListener <CheckPhotoResults>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall verifyUploadIdAsync(@NonNull String companyAPIKey,
                                                                @NonNull String accessCode,
                                                                @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyUploadId(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Check the current status of the asynchronous image processing on the server
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link CheckPhotoResults}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static CheckPhotoResults checkUploadPassport(@NonNull String companyAPIKey,
                                                        @NonNull String accessCode) throws AuthenticatingException {

        return doAction(myService.checkUploadPassport(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Verify the current status of the asynchronous image processing on the server
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link CheckPhotoResults}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyUploadPassport(@NonNull String companyAPIKey,
                                                      @NonNull String accessCode) throws AuthenticatingException {

        return doAction(myService.verifyUploadPassport(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Check the current status of the asynchronous image processing on the server
     *
     * @param listener      {@link AuthenticatingListener<CheckPhotoResults>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall checkUploadPassportAsync(@NonNull String companyAPIKey,
                                                                     @NonNull String accessCode,
                                                                     @NonNull AuthenticatingListener<CheckPhotoResults> listener) {
        return doAction(myService.checkUploadPassport(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Verify the current status of the asynchronous image processing on the server
     *
     * @param listener      {@link AuthenticatingListener<CheckPhotoResults>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall verifyUploadPassportAsync(@NonNull String companyAPIKey,
                                                                      @NonNull String accessCode,
                                                                      @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.verifyUploadPassport(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Get the networks available to you for the network verification test
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link AvailableNetworks}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static AvailableNetworks getAvailableNetworks(@NonNull String companyAPIKey,
                                                         @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getAvailableNetworks(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Get the networks available to you for the network verification test
     *
     * @param listener      {@link AuthenticatingListener<AvailableNetworks> listener}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall getAvailableNetworksAsync(@NonNull String companyAPIKey,
                                                                      @NonNull String accessCode,
                                                                      @NonNull AuthenticatingListener<AvailableNetworks> listener) {
        return doAction(myService.getAvailableNetworks(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Verify your social network. This should only be called after the user has successfully
     * logged into one of the available social OAuth platforms (IE: facebook) where you have obtained
     * their accessToken and id.
     *
     * @param companyAPIKey          The company api key provided by Authenticating
     * @param accessCode             The identifier String given to a user. Obtained when creating the user
     * @param network                String network, in lowercase, of the network they logged in on. Samples are:
     *                               faecbook, google, twitter, instagram
     * @param socialMediaAccessToken The access token you received from the social media login
     * @param socialMediaUserId      The user id you received from the social media login
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifySocialNetworks(String companyAPIKey,
                                                      String accessCode, String network,
                                                      String socialMediaAccessToken,
                                                      String socialMediaUserId) throws AuthenticatingException {
        SocialNetworkObj socialNetworkObj = new SocialNetworkObj();
        socialNetworkObj.setUserAccessCode(accessCode);
        socialNetworkObj.setNetwork(network);
        socialNetworkObj.setSocialMediaAccessToken(socialMediaAccessToken);
        socialNetworkObj.setSocialMediaUserId(socialMediaUserId);

        return doAction(myService.verifySocialNetworks(companyAPIKey, socialNetworkObj), accessCode);
    }

    /**
     * Verify your social network. This should only be called after the user has successfully
     * logged into one of the available social OAuth platforms (IE: facebook) where you have obtained
     * their accessToken and id.
     *
     * @param listener               {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey          The company api key provided by Authenticating
     * @param accessCode             The identifier String given to a user. Obtained when creating the user
     * @param network                String network, in lowercase, of the network they logged in on. Samples are:
     *                               faecbook, google, twitter, instagram
     * @param socialMediaAccessToken The access token you received from the social media login
     * @param socialMediaUserId      The user id you received from the social media login
     */
    public static AuthenticatingNetworkCall verifySocialNetworksAsync(@NonNull String companyAPIKey,
                                                                      @NonNull String accessCode, String network,
                                                                      @NonNull String socialMediaAccessToken,
                                                                      @NonNull String socialMediaUserId,
                                                                      @NonNull AuthenticatingListener<SimpleResponse> listener) {
        SocialNetworkObj socialNetworkObj = new SocialNetworkObj();
        socialNetworkObj.setUserAccessCode(accessCode);
        socialNetworkObj.setNetwork(network);
        socialNetworkObj.setSocialMediaAccessToken(socialMediaAccessToken);
        socialNetworkObj.setSocialMediaUserId(socialMediaUserId);
        return doAction(myService.verifySocialNetworks(companyAPIKey, socialNetworkObj), accessCode, listener);
    }

    /**
     * Get the quiz for the user to complete the verification proof test
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link QuizObject}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static QuizObject getQuiz(String companyAPIKey,
                                     String accessCode) throws AuthenticatingException {
        return doAction(myService.getQuiz(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Get the Quiz. Note, if you receive a response that contains an error and indicates that
     * information is missing (IE, address, last name, etc), just update the user via updateUser
     * and re-call this endpoint again.
     * Note! If no quiz can be generated after all information is filled out, it may be required
     * to include the User's Social Security at this point so as to generate correct quizData.
     * DO NOT PERSIST OR STORE THE USER'S SOCIAL SECURITY NUMBER IN ANY WAY
     *
     * @param listener      {@link AuthenticatingListener<QuizObject>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall getQuizAsync(@NonNull String companyAPIKey,
                                                         @NonNull String accessCode,
                                                         @NonNull AuthenticatingListener<QuizObject> listener) {
        return doAction(myService.getQuiz(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Verify the quiz by passing in the answers for the questions sent. The other information
     * listed here can be found in the original returned quiz object.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param answers       List of answer objects that contain the responses to the questions
     *                      that were obtained from the getQuiz
     * @param IDMSessionID  The quiz id. This is obtained from the {@link hotb.pgmacdesign.authenticatingsdk.datamodels.quiz.QuizData} obtained from getQuiz()
     *                      This is obtained from the {@link QuizObject} obtained from getQuiz()
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse verifyQuiz(@NonNull String companyAPIKey,
                                            @NonNull String accessCode,
                                            @NonNull List<QuestionAnswer> answers,
                                            @NonNull String IDMSessionID) throws AuthenticatingException {
        VerifyQuizObject verifyQuizObject = new VerifyQuizObject();
        verifyQuizObject.setUserAccessCode(accessCode);
        verifyQuizObject.setQuestionAnswers(answers);
        verifyQuizObject.setIDMSessionId(IDMSessionID);
        return doAction(myService.verifyQuiz(companyAPIKey, verifyQuizObject), accessCode);
    }

    /**
     * Verify the quiz by passing in the answers for the questions sent. The other information
     * listed here can be found in the original returned quiz object.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @param answers       List of answer objects that contain the responses to the questions
     *                      that were obtained from the getQuizAsync method
     * @param IDMSessionID  The quiz IDMSessionID. This is obtained from the {@link QuizObject} in the {@link hotb.pgmacdesign.authenticatingsdk.datamodels.quiz.QuizData}
     */
    public static AuthenticatingNetworkCall verifyQuizAsync(@NonNull String companyAPIKey,
                                                            @NonNull String accessCode, List<QuestionAnswer> answers,
                                                            @NonNull String IDMSessionID,
                                                            @NonNull AuthenticatingListener<SimpleResponse> listener) {

        VerifyQuizObject verifyQuizObject = new VerifyQuizObject();
        verifyQuizObject.setUserAccessCode(accessCode);
        verifyQuizObject.setQuestionAnswers(answers);
        verifyQuizObject.setIDMSessionId(IDMSessionID);
        return doAction(myService.verifyQuiz(companyAPIKey, verifyQuizObject), accessCode, listener);
    }

    /**
     * Generate a background report for a user. Keep in mind that a user must have
     * completed their identity quiz before attempting this else it will throw
     * an error upon calling.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link SimpleResponse}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static SimpleResponse generateBackgroundReport(@NonNull String companyAPIKey,
                                                          @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.generateCriminalReport(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Generate a background report for a user. Keep in mind that a user must have
     * completed their identity quiz before attempting this else it will throw
     * an error upon calling.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall generateBackgroundReportAsync(@NonNull String companyAPIKey,
                                                                          @NonNull String accessCode,
                                                                          @NonNull AuthenticatingListener<SimpleResponse> listener) {

        return doAction(myService.generateCriminalReport(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * @param companyAPIKey The company api key provided by Authenticating
     * @param user          The user object, minimum required keys are firstname, lastname, email and date of birth
     * @return {@link User} the saved user object
     * @throws AuthenticatingException Exception
     */
    public static User createUser(@NonNull String companyAPIKey, @NonNull User user) throws AuthenticatingException {
        if (isNullOrEmpty(user.getFirstName()))
            throw new AuthenticatingException("First Name is Required");
        else if (isNullOrEmpty(user.getLastName()))
            throw new AuthenticatingException("Last Name is Required");
        else if (isNullOrEmpty(user.getEmail()))
            throw new AuthenticatingException("Email is Required");
        else if (isNullOrEmpty(user.getDateOfBirth()))
            throw new AuthenticatingException("Date of Birth is Required");
        return doSynchronousTask(myService.createUser(companyAPIKey, user));
    }

    /**
     * Generate a background report for a user. Keep in mind that a user must have
     * completed their identity quiz before attempting this else it will throw
     * an error upon calling.
     *
     * @param listener      {@link AuthenticatingListener<SimpleResponse>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param user          The user object, minimum required keys are firstname, lastname, email and date of birth
     */
    public static AuthenticatingNetworkCall createUserAsync(@NonNull String companyAPIKey, @NonNull User user,
                                                            @NonNull AuthenticatingListener<User> listener) {
        if (isNullOrEmpty(user.getFirstName())) {
            listener.onError(new AuthenticatingException("First Name is Required"));
            return null;
        } else if (isNullOrEmpty(user.getLastName())) {
            listener.onError(new AuthenticatingException("Last Name is Required"));
            return null;
        } else if (isNullOrEmpty(user.getEmail())) {
            listener.onError(new AuthenticatingException("Email is Required"));
            return null;
        } else if (isNullOrEmpty(user.getDateOfBirth())) {
            listener.onError(new AuthenticatingException("Date of Birth is Required"));
            return null;
        }
        Call<User> call = myService.createUser(companyAPIKey, user);
        AuthenticatingNetworkCall authenticatingNetworkCall = new AuthenticatingNetworkCall(call);
        doAsynchronousTask(listener, call);
        return authenticatingNetworkCall;
    }

    /**
     * Generate a background report for a user. Keep in mind that a user must have
     * completed their identity quiz before attempting this else it will throw
     * an error upon calling.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     * @return {@link TestResult}
     * @throws AuthenticatingException {@link AuthenticatingException} Exception
     */
    public static TestResult getTestResult(@NonNull String companyAPIKey,
                                           @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.getTestResult(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Generate a background report for a user. Keep in mind that a user must have
     * completed their identity quiz before attempting this else it will throw
     * an error upon calling.
     *
     * @param listener      {@link AuthenticatingListener<TestResult>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall getTestResultAsync(@NonNull String companyAPIKey,
                                                               @NonNull String accessCode,
                                                               @NonNull AuthenticatingListener<TestResult> listener) {
        return doAction(myService.getTestResult(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    /**
     * Update user consent status
     * an error upon calling.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static SimpleResponse setUserConsent(@NonNull String companyAPIKey,
                                                @NonNull String accessCode,
                                                @NonNull String capturedName,
                                                boolean reportRequested,
                                                boolean isDisclosureAccepted) throws AuthenticatingException {
        return doAction(myService.setConsent(companyAPIKey, new ConsentRequest(accessCode, reportRequested ? 1 : 0, capturedName, isDisclosureAccepted ? 1 : 0)), accessCode);
    }

    /**
     * Update user consent status
     * an error upon calling.
     *
     * @param listener      {@link AuthenticatingListener<TestResult>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall setUserConsentAsync(@NonNull String companyAPIKey,
                                                                @NonNull String accessCode,
                                                                @NonNull String capturedName,
                                                                boolean reportRequested,
                                                                boolean isDisclosureAccepted,
                                                                @NonNull AuthenticatingListener<SimpleResponse> listener) {
        return doAction(myService.setConsent(companyAPIKey, new ConsentRequest(accessCode, reportRequested ? 1 : 0, capturedName, isDisclosureAccepted ? 1 : 0)), accessCode, listener);
    }
    /**
     * Gets the criminal report of the user back to seven years
     * an error upon calling.
     *
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static CriminalHistory get7YearCriminalHistory(@NonNull String companyAPIKey,
                                                          @NonNull String accessCode) throws AuthenticatingException {
        return doAction(myService.get7YearCriminalHistory(companyAPIKey, new SimpleRequest(accessCode)), accessCode);
    }

    /**
     * Gets the criminal report of the user back to seven years
     * an error upon calling.
     *
     * @param listener      {@link AuthenticatingListener<TestResult>}
     * @param companyAPIKey The company api key provided by Authenticating
     * @param accessCode    The identifier String given to a user. Obtained when creating the user
     */
    public static AuthenticatingNetworkCall get7YearCriminalHistoryAsync(@NonNull String companyAPIKey,
                                                                @NonNull String accessCode,
                                                                @NonNull AuthenticatingListener<CriminalHistory> listener) {
        return doAction(myService.get7YearCriminalHistory(companyAPIKey, new SimpleRequest(accessCode)), accessCode, listener);
    }

    public enum UploadIdTypes {
        uploadId, uploadIdEnhanced, comparePhotos, uploadPassport
    }

}