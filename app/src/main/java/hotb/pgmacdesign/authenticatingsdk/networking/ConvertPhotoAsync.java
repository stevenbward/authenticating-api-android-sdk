package hotb.pgmacdesign.authenticatingsdk.networking;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

import hotb.pgmacdesign.authenticatingsdk.datamodels.AuthenticatingException;
import hotb.pgmacdesign.authenticatingsdk.datamodels.UploadPhotosObj;
import hotb.pgmacdesign.authenticatingsdk.interfaces.AuthenticatingListener;

@SuppressWarnings("WeakerAccess")
public class ConvertPhotoAsync extends AsyncTask<Void, Integer, UploadPhotosObj> {

    /**
     * 2 Megabyte cap on incoming images. Larger ones will be resized down. Anything <2mb will be
     * sent in without any image pre-processing
     */
    private static final float MAX_SIZE_IMAGE_UPLOAD = 2000000;
    private Bitmap bitmap1OrIDFront, bitmap2OrIDBack;
    private AuthenticatingListener<UploadPhotosObj> listener;
    private AuthenticatingAPICalls.UploadIdTypes type;

    //Error Objects
    private AuthenticatingException error;

    public ConvertPhotoAsync(@NonNull Bitmap bitmap1OrIDFront, @Nullable Bitmap bitmap2OrIDBack,
                             @NonNull AuthenticatingAPICalls.UploadIdTypes type,
                             @NonNull AuthenticatingListener<UploadPhotosObj> listener) {
        this.bitmap1OrIDFront = bitmap1OrIDFront;
        this.bitmap2OrIDBack = bitmap2OrIDBack;
        this.listener = listener;
        this.type = type;
        this.error = null;
    }

    /**
     * Determine if a bitmap is too large as compared to passed param
     *
     * @param bmp Bitmap to check
     * @return boolean, if true, bitmap is larger than the desired size, else, it is not.
     */
    static boolean isBitmapTooLarge(@NonNull Bitmap bmp) {
        long bitmapSize = bmp.getByteCount();
        float shrinkFactor = MAX_SIZE_IMAGE_UPLOAD / bitmapSize;
        return !(shrinkFactor >= 1);
    }

    /**
     * Resize a photo
     *
     * @param bmp Bitmap to resize
     * @return Resized bitmap. If it fails, will send back original
     */
    static Bitmap resizePhoto(@NonNull Bitmap bmp) throws AuthenticatingException {
        try {
            double height = Math.sqrt(MAX_SIZE_IMAGE_UPLOAD /
                    (((double) bmp.getWidth()) / bmp.getHeight()));
            double width = (height / bmp.getHeight()) * bmp.getWidth();
            return Bitmap.createScaledBitmap(bmp, (int) (width),
                    (int) (height), true);
        } catch (Exception e) {
            throw new AuthenticatingException(e);
        }
    }

    static String encodeImage(@NonNull Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    @Override
    protected UploadPhotosObj doInBackground(Void... params) {
        UploadPhotosObj uploadPhotosObj = new UploadPhotosObj();

        try {
            switch (type) {
                case uploadPassport:
                    uploadPhotosObj.setIdFront(shrinkAndEncodeImage(bitmap1OrIDFront));
                    break;
                case uploadIdEnhanced:
                case uploadId:
                    uploadPhotosObj.setIdFront(shrinkAndEncodeImage(bitmap1OrIDFront));
                    uploadPhotosObj.setIdBack(shrinkAndEncodeImage(bitmap2OrIDBack));
                    break;
                case comparePhotos:
                    uploadPhotosObj.setImg1(shrinkAndEncodeImage(bitmap1OrIDFront));
                    uploadPhotosObj.setImg2(shrinkAndEncodeImage(bitmap2OrIDBack));
                    break;
            }
        } catch (AuthenticatingException ex) {
            error = ex;
        }

        return uploadPhotosObj;
    }

    @Override
    protected void onPostExecute(UploadPhotosObj uploadPhotosObj) {
        if (error == null) listener.onSuccess(uploadPhotosObj);
        else listener.onError(error);
    }

    private String shrinkAndEncodeImage(Bitmap bitmapToProcess) throws AuthenticatingException {

        if (bitmapToProcess == null)
            throw new AuthenticatingException("One or more bitmap was null");

        Bitmap bitmap;
        String stringOutput = null;
        try {
            if (isBitmapTooLarge(bitmapToProcess))
                bitmap = resizePhoto(bitmapToProcess);
            else bitmap = Bitmap.createBitmap(bitmapToProcess);
        } catch (OutOfMemoryError oom) {
            //File too large, resize to very small
            bitmap = shrinkPhoto(bitmapToProcess);
        }

        try {
            if (bitmap != null) {
                stringOutput = encodeImage(bitmap);
                bitmap.recycle();
            }
            if (stringOutput == null || stringOutput.isEmpty())
                throw new AuthenticatingException("Could not convert images to base64");
            return stringOutput;
        } catch (Exception e) {
            throw new AuthenticatingException(e);
        }
    }

    private Bitmap shrinkPhoto(@NonNull Bitmap bmp) throws AuthenticatingException {
        int factorToDivide = 8;
        try {
            return Bitmap.createScaledBitmap(bmp, (bmp.getWidth() / factorToDivide),
                    (bmp.getHeight() / factorToDivide), true);
        } catch (Exception e) {
            throw new AuthenticatingException(e);
        }
    }

}
