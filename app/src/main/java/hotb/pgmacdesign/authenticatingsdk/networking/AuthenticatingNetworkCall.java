package hotb.pgmacdesign.authenticatingsdk.networking;

import retrofit2.Call;

@SuppressWarnings("WeakerAccess")
public class AuthenticatingNetworkCall {
    private Call<?> call;
    private boolean cancelled = false;

    AuthenticatingNetworkCall() {
    }

    public AuthenticatingNetworkCall(Call<?> call) {
        this.call = call;
    }

    public void cancelApiCall() {
        if (call != null) call.cancel();
        cancelled = true;
    }

    void setCall(Call<?> call) {
        if (cancelled) call.cancel();
        this.call = call;
    }
}
