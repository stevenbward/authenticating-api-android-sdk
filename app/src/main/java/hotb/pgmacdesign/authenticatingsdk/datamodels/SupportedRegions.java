package hotb.pgmacdesign.authenticatingsdk.datamodels;

public enum SupportedRegions {
    UNITED_STATES,
    AUSTRALIA,
    ASIA,
    CANADA,
    AMERICA,
    EUROPE,
    AFRICA,
    GENERAL_DOCUMENTS
}
