package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pmacdowell on 2017-07-25.
 */

public class SimpleResponse extends ErrorParsingObj {

    @SerializedName("successful")
    private Boolean successful;

    public boolean getSuccessful() {
        boolean successPrimitive;
        if (successful == null) successPrimitive = false;
        else successPrimitive = successful;
        return successPrimitive;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
