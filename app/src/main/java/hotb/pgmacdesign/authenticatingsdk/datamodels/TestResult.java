package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by pmacdowell on 2018-01-18.
 */

public class TestResult {

    private Boolean successful;
    private String note;
    private Company company;
    private User user;
    private ContactProof contactProof;
    private SocialProof socialProof;
    private PhotoProof photoProof;
    private IdentityProof identityProof;
    private CanadaResults canadaResults;
    private BackgroundCheck backgroundCheck;
    private ScannedUser scannedUser;
    private PassportData passportData;
    @SerializedName("consent")
    private Consent consent;

    public Consent getConsent() {
        return consent;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public PassportData getPassportData() {
        return passportData;
    }

    public void setPassportData(PassportData passportData) {
        this.passportData = passportData;
    }

    public ScannedUser getScannedUser() {
        return scannedUser;
    }

    public void setScannedUser(ScannedUser scannedUser) {
        this.scannedUser = scannedUser;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ContactProof getContactProof() {
        return contactProof;
    }

    public void setContactProof(ContactProof contactProof) {
        this.contactProof = contactProof;
    }

    public SocialProof getSocialProof() {
        return socialProof;
    }

    public void setSocialProof(SocialProof socialProof) {
        this.socialProof = socialProof;
    }

    public PhotoProof getPhotoProof() {
        return photoProof;
    }

    public void setPhotoProof(PhotoProof photoProof) {
        this.photoProof = photoProof;
    }

    public IdentityProof getIdentityProof() {
        return identityProof;
    }

    public void setIdentityProof(IdentityProof identityProof) {
        this.identityProof = identityProof;
    }

    public CanadaResults getCanadaResults() {
        return canadaResults;
    }

    public void setCanadaResults(CanadaResults canadaResults) {
        this.canadaResults = canadaResults;
    }

    public BackgroundCheck getBackgroundCheck() {
        return backgroundCheck;
    }

    public void setBackgroundCheck(BackgroundCheck backgroundCheck) {
        this.backgroundCheck = backgroundCheck;
    }

    public static class PassportData {
        private String Address2;
        private String Address3;
        private String Country;
        private String DateOfBirth;
        private String DateOfBirth4;
        private String ExpirationDate;
        private String ExpirationDate4;
        private String NameFirst;
        private String NameLast;
        private String PassportNumber;
        private String WebResponseDescription;
        @SerializedName("AuthenticationResult")
        private String authenticationResult;
        @SerializedName("AuthenticationObject")
        private String authenticationObject;
        @SerializedName("AuthenticationResultSummary")
        private ArrayList<String> authenticationResultSummary;

        public ArrayList<String> getAuthenticationResultSummary() {
            return authenticationResultSummary;
        }

        public String getAuthenticationObject() {
            return authenticationObject;
        }

        public String getAuthenticationResult() {
            return authenticationResult;
        }

        public String getAddress2() {
            return Address2;
        }

        public void setAddress2(String address2) {
            Address2 = address2;
        }

        public String getAddress3() {
            return Address3;
        }

        public void setAddress3(String address3) {
            Address3 = address3;
        }

        public String getCountry() {
            return Country;
        }

        public void setCountry(String country) {
            Country = country;
        }

        public String getDateOfBirth() {
            return DateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            DateOfBirth = dateOfBirth;
        }

        public String getDateOfBirth4() {
            return DateOfBirth4;
        }

        public void setDateOfBirth4(String dateOfBirth4) {
            DateOfBirth4 = dateOfBirth4;
        }

        public String getExpirationDate() {
            return ExpirationDate;
        }

        public void setExpirationDate(String expirationDate) {
            ExpirationDate = expirationDate;
        }

        public String getExpirationDate4() {
            return ExpirationDate4;
        }

        public void setExpirationDate4(String expirationDate4) {
            ExpirationDate4 = expirationDate4;
        }

        public String getNameFirst() {
            return NameFirst;
        }

        public void setNameFirst(String nameFirst) {
            NameFirst = nameFirst;
        }

        public String getNameLast() {
            return NameLast;
        }

        public void setNameLast(String nameLast) {
            NameLast = nameLast;
        }

        public String getPassportNumber() {
            return PassportNumber;
        }

        public void setPassportNumber(String passportNumber) {
            PassportNumber = passportNumber;
        }

        public String getWebResponseDescription() {
            return WebResponseDescription;
        }

        public void setWebResponseDescription(String webResponseDescription) {
            WebResponseDescription = webResponseDescription;
        }
    }

    public static class ScannedUser {

        private String Address;
        private String City;
        private String Class;
        private String CountryShort;
        private String DateOfBirth;
        private String DateOfBirth4;
        private String Endorsements;
        private String ExpirationDate;
        private String ExpirationDate4;
        private String Eyes;
        private String Hair;
        private String Height;
        private String Id;
        private String IdCountry;
        private String IssueDate;
        private String IssueDate4;
        private String NameFirst;
        private String NameLast;
        private String NameMiddle;
        private String Restriction;
        private String Sex;
        private String State;
        private String Weight;
        private String Zip;
        private String license;
        private String CardType;
        private String TemplateType;
        @SerializedName("IsAddressCorrected")
        private Boolean isAddressCorrected;
        @SerializedName("IsAddressVerified")
        private Boolean isAddressVerified;
        @SerializedName("IsBarcodeRead")
        private Boolean isbarcodeRead;
        @SerializedName("IsIDVerified")
        private Boolean isIdVerified;
        @SerializedName("IsOcrRead")
        private Boolean isOCRRead;
        @SerializedName("DocumentDetectedName")
        private String documentDetectedName;
        @SerializedName("DocumentDetectedNameShort")
        private String documentDetectedNameShort;
        @SerializedName("DocumentVerificationConfidenceRating")
        private Integer documentVerififationConfidenceRating;
        @SerializedName("AuthenticationResult")
        private String authenticationResult;
        @SerializedName("AuthenticationObject")
        private String authenticationObject;
        @SerializedName("AuthenticationResultSummary")
        private ArrayList<String> authenticationResultSummary;

        public String getAuthenticationResult() {
            return authenticationResult;
        }

        public String getAuthenticationObject() {
            return authenticationObject;
        }

        public ArrayList<String> getAuthenticationResultSummary() {
            return authenticationResultSummary;
        }

        public Boolean getAddressCorrected() {
            return isAddressCorrected;
        }

        public Boolean getAddressVerified() {
            return isAddressVerified;
        }

        public Boolean getIsbarcodeRead() {
            return isbarcodeRead;
        }

        public Boolean getIdVerified() {
            return isIdVerified;
        }

        public Boolean getOCRRead() {
            return isOCRRead;
        }

        public String getDocumentDetectedName() {
            return documentDetectedName;
        }

        public String getDocumentDetectedNameShort() {
            return documentDetectedNameShort;
        }

        public Integer getDocumentVerififationConfidenceRating() {
            return documentVerififationConfidenceRating;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String city) {
            City = city;
        }

        public String getTheClass() {
            return this.Class;
        }

        public void setClass(String aClass) {
            Class = aClass;
        }

        public String getCountryShort() {
            return CountryShort;
        }

        public void setCountryShort(String countryShort) {
            CountryShort = countryShort;
        }

        public String getDateOfBirth() {
            return DateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            DateOfBirth = dateOfBirth;
        }

        public String getDateOfBirth4() {
            return DateOfBirth4;
        }

        public void setDateOfBirth4(String dateOfBirth4) {
            DateOfBirth4 = dateOfBirth4;
        }

        public String getEndorsements() {
            return Endorsements;
        }

        public void setEndorsements(String endorsements) {
            Endorsements = endorsements;
        }

        public String getExpirationDate() {
            return ExpirationDate;
        }

        public void setExpirationDate(String expirationDate) {
            ExpirationDate = expirationDate;
        }

        public String getExpirationDate4() {
            return ExpirationDate4;
        }

        public void setExpirationDate4(String expirationDate4) {
            ExpirationDate4 = expirationDate4;
        }

        public String getEyes() {
            return Eyes;
        }

        public void setEyes(String eyes) {
            Eyes = eyes;
        }

        public String getHair() {
            return Hair;
        }

        public void setHair(String hair) {
            Hair = hair;
        }

        public String getHeight() {
            return Height;
        }

        public void setHeight(String height) {
            Height = height;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getIdCountry() {
            return IdCountry;
        }

        public void setIdCountry(String idCountry) {
            IdCountry = idCountry;
        }

        public String getIssueDate() {
            return IssueDate;
        }

        public void setIssueDate(String issueDate) {
            IssueDate = issueDate;
        }

        public String getIssueDate4() {
            return IssueDate4;
        }

        public void setIssueDate4(String issueDate4) {
            IssueDate4 = issueDate4;
        }

        public String getNameFirst() {
            return NameFirst;
        }

        public void setNameFirst(String nameFirst) {
            NameFirst = nameFirst;
        }

        public String getNameLast() {
            return NameLast;
        }

        public void setNameLast(String nameLast) {
            NameLast = nameLast;
        }

        public String getNameMiddle() {
            return NameMiddle;
        }

        public void setNameMiddle(String nameMiddle) {
            NameMiddle = nameMiddle;
        }

        public String getRestriction() {
            return Restriction;
        }

        public void setRestriction(String restriction) {
            Restriction = restriction;
        }

        public String getSex() {
            return Sex;
        }

        public void setSex(String sex) {
            Sex = sex;
        }

        public String getState() {
            return State;
        }

        public void setState(String state) {
            State = state;
        }

        public String getWeight() {
            return Weight;
        }

        public void setWeight(String weight) {
            Weight = weight;
        }

        public String getZip() {
            return Zip;
        }

        public void setZip(String zip) {
            Zip = zip;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getCardType() {
            return CardType;
        }

        public void setCardType(String cardType) {
            CardType = cardType;
        }

        public String getTemplateType() {
            return TemplateType;
        }

        public void setTemplateType(String templateType) {
            TemplateType = templateType;
        }
    }

    public static class BackgroundCheck {
        private Boolean hasCriminalRecord;

        public Boolean isHasCriminalRecord() {
            return hasCriminalRecord;
        }

        public void setHasCriminalRecord(Boolean hasCriminalRecord) {
            this.hasCriminalRecord = hasCriminalRecord;
        }
    }

    public static class ContactProof {
        private boolean verifiedPhone;
        private boolean verifiedEmail;

        public boolean isVerifiedPhone() {
            return verifiedPhone;
        }

        public void setVerifiedPhone(boolean verifiedPhone) {
            this.verifiedPhone = verifiedPhone;
        }

        public boolean isVerifiedEmail() {
            return verifiedEmail;
        }

        public void setVerifiedEmail(boolean verifiedEmail) {
            this.verifiedEmail = verifiedEmail;
        }
    }

    public static class SocialProof {
        private boolean successfulLogin;

        public boolean isSuccessfulLogin() {
            return successfulLogin;
        }

        public void setSuccessfulLogin(boolean successfulLogin) {
            this.successfulLogin = successfulLogin;
        }
    }

    public static class PhotoProof {
        private Float score;

        public Float getScore() {
            return score;
        }

        public void setScore(Float score) {
            this.score = score;
        }
    }

    public static class IdentityProof {
        private Float score;
        private Integer numQuestions;
        private String status;
        private String description;

        public Integer getNumQuestions() {
            return numQuestions;
        }

        public void setNumQuestions(Integer numQuestions) {
            this.numQuestions = numQuestions;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Float getScore() {
            return score;
        }

        public void setScore(Float score) {
            this.score = score;
        }
    }

    public static class CanadaResults {
        private String userValidation;
        private String note;

        public String getUserValidation() {
            return userValidation;
        }

        public void setUserValidation(String userValidation) {
            this.userValidation = userValidation;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }

    public static class Consent {
        @SerializedName("is_report_checked")
        private Integer isReportChecked;
        @SerializedName("background_check_disclosure_accepted")
        private Integer backgroundCheckDisclosureAccepted;

        public Integer getIsReportChecked() {
            return isReportChecked;
        }

        public Integer getBackgroundCheckDisclosureAccepted() {
            return backgroundCheckDisclosureAccepted;
        }
    }

    public class AuthenticationObject {

        @SerializedName("Biographic")
        private Biographic biographic;

        public Biographic getBiographic() {
            return biographic;
        }
    }

    public class Biographic {
        @SerializedName("Age")
        private Integer age;
        @SerializedName("BirthDate")
        private String birthDate;
        @SerializedName("ExpirationDate")
        private String expirationDate;
        @SerializedName("FullName")
        private String fullName;
        @SerializedName("Gender")
        private Integer gender;
        @SerializedName("Photo")
        private String photo;

        public Integer getAge() {
            return age;
        }

        public String getBirthDate() {
            return birthDate;
        }

        public String getExpirationDate() {
            return expirationDate;
        }

        public String getFullName() {
            return fullName;
        }

        public Integer getGender() {
            return gender;
        }

        public String getPhoto() {
            return photo;
        }
    }
}
