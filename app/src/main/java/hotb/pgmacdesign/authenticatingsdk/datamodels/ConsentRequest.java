package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

public class ConsentRequest extends SimpleRequest {

    @SerializedName("isReportChecked")
    private int isReportChecked;
    @SerializedName("fullName")
    private String enteredName;
    @SerializedName("isBackgroundDisclosureAccepted")
    private int isBackgroundDisclosureAccepted;

    public ConsentRequest(String userAccessCode, int isReportChecked, String enteredName, int isBackgroundDisclosureAccepted) {
        super(userAccessCode);
        this.isReportChecked = isReportChecked;
        this.enteredName = enteredName;
        this.isBackgroundDisclosureAccepted = isBackgroundDisclosureAccepted;
    }
}
