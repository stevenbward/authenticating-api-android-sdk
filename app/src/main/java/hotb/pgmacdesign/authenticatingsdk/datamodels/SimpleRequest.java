package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

public class SimpleRequest extends ErrorParsingObj {
    @SerializedName("userAccessCode")
    private String userAccessCode;

    public SimpleRequest(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }

    public SimpleRequest() {
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }
}
