package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Offenses {

    @SerializedName("Offense")
    private List<Offense> offense = null;

    public List<Offense> getOffense() {
        return offense;
    }

    public void setOffense(List<Offense> offense) {
        this.offense = offense;
    }

}