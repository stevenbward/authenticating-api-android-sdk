package hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Education {
    @SerializedName("schoolName")
    private String schoolName;
    @SerializedName("schoolType")
    private String schoolType;
    @SerializedName("institutionCampusName")
    private String institutionCampusName;
    @SerializedName("institutionPhone")
    private String institutionPhone;
    @SerializedName("institutionAddress")
    private String institutionAddress;
    @SerializedName("institutionAddressTown")
    private String institutionAddressTown;
    @SerializedName("institutionAddressCountry")
    private String institutionAddressCountry;
    @SerializedName("institutionAddressStateProvince")
    private String institutionAddressStateProvince;
    @SerializedName("institutionAddressPostalCode")
    private String institutionAddressPostalCode;
    @SerializedName("startDate")
    private String startDate;
    @SerializedName("endDate")
    private String endDate;
    @SerializedName("degreeTitle")
    private String degreeTitle;
    @SerializedName("currentlyAttending")
    private String currentlyAttending;
    @SerializedName("CompletedSuccessfully")
    private String completedSuccessfully;
    @SerializedName("majors")
    private List<String> majors = null;
    @SerializedName("minors")
    private List<String> minors = null;
    @SerializedName("honors")
    private String honors;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(String schoolType) {
        this.schoolType = schoolType;
    }

    public String getInstitutionCampusName() {
        return institutionCampusName;
    }

    public void setInstitutionCampusName(String institutionCampusName) {
        this.institutionCampusName = institutionCampusName;
    }

    public String getInstitutionPhone() {
        return institutionPhone;
    }

    public void setInstitutionPhone(String institutionPhone) {
        this.institutionPhone = institutionPhone;
    }

    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    public String getInstitutionAddressTown() {
        return institutionAddressTown;
    }

    public void setInstitutionAddressTown(String institutionAddressTown) {
        this.institutionAddressTown = institutionAddressTown;
    }

    public String getInstitutionAddressCountry() {
        return institutionAddressCountry;
    }

    public void setInstitutionAddressCountry(String institutionAddressCountry) {
        this.institutionAddressCountry = institutionAddressCountry;
    }

    public String getInstitutionAddressStateProvince() {
        return institutionAddressStateProvince;
    }

    public void setInstitutionAddressStateProvince(String institutionAddressStateProvince) {
        this.institutionAddressStateProvince = institutionAddressStateProvince;
    }

    public String getInstitutionAddressPostalCode() {
        return institutionAddressPostalCode;
    }

    public void setInstitutionAddressPostalCode(String institutionAddressPostalCode) {
        this.institutionAddressPostalCode = institutionAddressPostalCode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDegreeTitle() {
        return degreeTitle;
    }

    public void setDegreeTitle(String degreeTitle) {
        this.degreeTitle = degreeTitle;
    }

    public String getCurrentlyAttending() {
        return currentlyAttending;
    }

    public void setCurrentlyAttending(String currentlyAttending) {
        this.currentlyAttending = currentlyAttending;
    }

    public String getCompletedSuccessfully() {
        return completedSuccessfully;
    }

    public void setCompletedSuccessfully(String completedSuccessfully) {
        this.completedSuccessfully = completedSuccessfully;
    }

    public List<String> getMajors() {
        return majors;
    }

    public void setMajors(List<String> majors) {
        this.majors = majors;
    }

    public List<String> getMinors() {
        return minors;
    }

    public void setMinors(List<String> minors) {
        this.minors = minors;
    }

    public String getHonors() {
        return honors;
    }

    public void setHonors(String honors) {
        this.honors = honors;
    }
}
