
package hotb.pgmacdesign.authenticatingsdk.datamodels.quiz;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hotb.pgmacdesign.authenticatingsdk.datamodels.ErrorParsingObj;

public class QuizObject extends ErrorParsingObj {

    @SerializedName("data")
    @Expose
    private QuizData quizData;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public QuizData getQuizData() {
        return quizData;
    }

    public void setQuizData(QuizData quizData) {
        this.quizData = quizData;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
