package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import com.google.gson.annotations.SerializedName;

public class Subject {

    @SerializedName("FullName")
    private String fullName;
    @SerializedName("DOB")
    private String dOB;
    @SerializedName("Image")
    private String image;
    @SerializedName("Category")
    private String category;
    @SerializedName("Source")
    private String source;
    @SerializedName("Sex")
    private String sex;
    @SerializedName("Race")
    private String race;
    @SerializedName("HairColor")
    private String hairColor;
    @SerializedName("EyeColor")
    private String eyeColor;
    @SerializedName("Weight")
    private String weight;
    @SerializedName("Height")
    private String height;
    @SerializedName("Age")
    private Integer age;
    @SerializedName("CaseNumber")
    private Integer caseNumber;
    @SerializedName("State")
    private String state;
    @SerializedName("Address")
    private String address;
    @SerializedName("Comments")
    private String comments;
    @SerializedName("Alias")
    private String alias;
    @SerializedName("ScarsMarks")
    private String scarsMarks;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDOB() {
        return dOB;
    }

    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(Integer caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getScarsMarks() {
        return scarsMarks;
    }

    public void setScarsMarks(String scarsMarks) {
        this.scarsMarks = scarsMarks;
    }

}
