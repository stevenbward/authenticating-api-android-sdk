package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("Candidates")
    private Candidates candidates;

    public Candidates getCandidates() {
        return candidates;
    }

    public void setCandidates(Candidates candidates) {
        this.candidates = candidates;
    }

}