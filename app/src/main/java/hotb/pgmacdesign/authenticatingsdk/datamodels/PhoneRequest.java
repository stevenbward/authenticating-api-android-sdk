package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

public class PhoneRequest extends SimpleRequest {
    @SerializedName("phone")
    private String phone;

    public PhoneRequest(String userAccessCode, String phone) {
        super(userAccessCode);
        this.phone = phone;
    }
}
