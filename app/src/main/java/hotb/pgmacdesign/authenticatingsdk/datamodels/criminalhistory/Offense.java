package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import com.google.gson.annotations.SerializedName;

public class Offense {

    @SerializedName("Description")
    private String description;
    @SerializedName("Disposition")
    private String disposition;
    @SerializedName("DispositionDate")
    private String dispositionDate;
    @SerializedName("OffenseDate")
    private String offenseDate;
    @SerializedName("CommitmentDate")
    private String commitmentDate;
    @SerializedName("ReleaseDate")
    private String releaseDate;
    @SerializedName("Statute")
    private String statute;
    @SerializedName("ConvictionDate")
    private String convictionDate;
    @SerializedName("ConvictionLocation")
    private String convictionLocation;

    public String getStatute() {
        return statute;
    }

    public void setStatute(String statute) {
        this.statute = statute;
    }

    public String getConvictionDate() {
        return convictionDate;
    }

    public void setConvictionDate(String convictionDate) {
        this.convictionDate = convictionDate;
    }

    public String getConvictionLocation() {
        return convictionLocation;
    }

    public void setConvictionLocation(String convictionLocation) {
        this.convictionLocation = convictionLocation;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getDispositionDate() {
        return dispositionDate;
    }

    public void setDispositionDate(String dispositionDate) {
        this.dispositionDate = dispositionDate;
    }

    public String getOffenseDate() {
        return offenseDate;
    }

    public void setOffenseDate(String offenseDate) {
        this.offenseDate = offenseDate;
    }

    public String getCommitmentDate() {
        return commitmentDate;
    }

    public void setCommitmentDate(String commitmentDate) {
        this.commitmentDate = commitmentDate;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

}