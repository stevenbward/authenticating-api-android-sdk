package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import com.google.gson.annotations.SerializedName;

public class Candidate {

    @SerializedName("Subject")
    private Subject subject;
    @SerializedName("Offenses")
    private Offenses offenses;

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Offenses getOffenses() {
        return offenses;
    }

    public void setOffenses(Offenses offenses) {
        this.offenses = offenses;
    }

}