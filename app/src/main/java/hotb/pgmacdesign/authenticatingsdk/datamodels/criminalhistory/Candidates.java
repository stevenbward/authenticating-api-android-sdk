package hotb.pgmacdesign.authenticatingsdk.datamodels.criminalhistory;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Candidates {

    @SerializedName("Candidate")
    private List<Candidate> candidate = null;
    @SerializedName("Message")
    private String message = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Candidate> getCandidate() {
        return candidate;
    }

    public void setCandidate(List<Candidate> candidate) {
        this.candidate = candidate;
    }

}
