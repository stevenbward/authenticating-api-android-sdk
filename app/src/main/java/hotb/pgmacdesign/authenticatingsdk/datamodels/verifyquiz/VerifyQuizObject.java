
package hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleRequest;

public class VerifyQuizObject extends SimpleRequest {

    @SerializedName("IDMSessionId")
    private String iDMSessionId;
    @SerializedName("questionAnswers")
    private List<QuestionAnswer> questionAnswers = null;

    public String getiDMSessionId() {
        return iDMSessionId;
    }

    public void setIDMSessionId(String iDMSessionId) {
        this.iDMSessionId = iDMSessionId;
    }

    public List<QuestionAnswer> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<QuestionAnswer> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }
}
