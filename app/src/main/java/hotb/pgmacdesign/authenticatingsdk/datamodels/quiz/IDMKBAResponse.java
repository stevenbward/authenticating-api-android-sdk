
package hotb.pgmacdesign.authenticatingsdk.datamodels.quiz;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IDMKBAResponse {

    @SerializedName("KBAQuestion")
    @Expose
    public List<KBAQuestion> kBAQuestion = null;
    @SerializedName("KBAStatus")
    @Expose
    public KBAStatus kBAStatus;
    @SerializedName("IsKbaEnabled")
    @Expose
    public String isKbaEnabled;

}
