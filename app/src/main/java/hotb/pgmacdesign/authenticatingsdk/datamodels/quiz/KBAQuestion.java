
package hotb.pgmacdesign.authenticatingsdk.datamodels.quiz;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KBAQuestion {

    @SerializedName("QuestionId")
    private Integer questionId;
    @SerializedName("Question")
    private String question;
    @SerializedName("QuestionType")
    private String questionType;
    @SerializedName("Options")
    private List<Option> options = null;

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}
