
package hotb.pgmacdesign.authenticatingsdk.datamodels.quiz;

import com.google.gson.annotations.SerializedName;

public class QuizData {

    @SerializedName("IDMSessionId")
    private String iDMSessionId;
    @SerializedName("IDMKBAResponse")
    private IDMKBAResponse iDMKBAResponse;

    public String getiDMSessionId() {
        return iDMSessionId;
    }

    public void setiDMSessionId(String iDMSessionId) {
        this.iDMSessionId = iDMSessionId;
    }

    public IDMKBAResponse getiDMKBAResponse() {
        return iDMKBAResponse;
    }

    public void setiDMKBAResponse(IDMKBAResponse iDMKBAResponse) {
        this.iDMKBAResponse = iDMKBAResponse;
    }
}
