
package hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hotb.pgmacdesign.authenticatingsdk.datamodels.SimpleRequest;

public class EmploymentVerificationRequest extends SimpleRequest {

    @SerializedName("employmentDetail")
    private List<EmploymentDetail> employmentDetail = null;

    public EmploymentVerificationRequest(String userAccessCode, List<EmploymentDetail> employmentDetail) {
        super(userAccessCode);
        this.employmentDetail = employmentDetail;
    }

    public List<EmploymentDetail> getEmploymentDetail() {
        return employmentDetail;
    }

    public void setEmploymentDetail(List<EmploymentDetail> employmentDetail) {
        this.employmentDetail = employmentDetail;
    }

}
