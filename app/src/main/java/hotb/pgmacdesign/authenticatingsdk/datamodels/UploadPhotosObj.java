package hotb.pgmacdesign.authenticatingsdk.datamodels;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;

/**
 * Used in the comparePhotos() endpoint.
 * {@link hotb.pgmacdesign.authenticatingsdk.networking.AuthenticatingAPICalls#comparePhotos(String, String, Bitmap, Bitmap)}
 * Created by pmacdowell on 2017-08-14.
 */

public class UploadPhotosObj extends SimpleRequest {

    @SerializedName("img1")
    private String img1;
    @SerializedName("img2")
    private String img2;
    @SerializedName("idFront")
    private String idFront;
    @SerializedName("idBack")
    private String idBack;
    @SerializedName("country")
    private Integer country;

    public Integer getCountry() {
        return country;
    }

    public void setCountry(Integer country) {
        this.country = country;
    }

    public void setIdFront(String idFront) {
        this.idFront = idFront;
    }

    public void setIdBack(String idBack) {
        this.idBack = idBack;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg1() {
        return img1;
    }

    public String getImg2() {
        return img2;
    }

    public String getIdFront() {
        return idFront;
    }

    public String getIdBack() {
        return idBack;
    }
}
