package hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification;

import com.google.gson.annotations.SerializedName;

import hotb.pgmacdesign.authenticatingsdk.datamodels.ErrorParsingObj;

public class EmploymentStatusResult extends ErrorParsingObj {

    @SerializedName("employer_name")
    private String employerName;
    @SerializedName("employer_name_verified")
    private Boolean employerNameVerified;
    @SerializedName("employer_address")
    private String employerAddress;
    @SerializedName("employer_address_verified")
    private Boolean employerAddressVerified;
    @SerializedName("employer_town")
    private String employerTown;
    @SerializedName("employer_town_verified")
    private Boolean employerTownVerified;
    @SerializedName("employer_country")
    private String employerCountry;
    @SerializedName("employer_country_verified")
    private Boolean employerCountryVerified;
    @SerializedName("job_title")
    private String jobTitle;
    @SerializedName("job_title_verified")
    private Boolean jobTitleVerified;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("start_date_verified")
    private Boolean startDateVerified;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("end_date_verified")
    private Boolean endDateVerified;
    @SerializedName("current_employment")
    private String currentEmployment;
    @SerializedName("current_employment_verified")
    private Boolean currentEmploymentVerified;
    @SerializedName("contract_type")
    private String contractType;
    @SerializedName("contract_type_verified")
    private Boolean contractTypeVerified;
    @SerializedName("status")
    private String status;

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public Boolean getEmployerNameVerified() {
        return employerNameVerified;
    }

    public void setEmployerNameVerified(Boolean employerNameVerified) {
        this.employerNameVerified = employerNameVerified;
    }

    public String getEmployerAddress() {
        return employerAddress;
    }

    public void setEmployerAddress(String employerAddress) {
        this.employerAddress = employerAddress;
    }

    public Boolean getEmployerAddressVerified() {
        return employerAddressVerified;
    }

    public void setEmployerAddressVerified(Boolean employerAddressVerified) {
        this.employerAddressVerified = employerAddressVerified;
    }

    public String getEmployerTown() {
        return employerTown;
    }

    public void setEmployerTown(String employerTown) {
        this.employerTown = employerTown;
    }

    public Boolean getEmployerTownVerified() {
        return employerTownVerified;
    }

    public void setEmployerTownVerified(Boolean employerTownVerified) {
        this.employerTownVerified = employerTownVerified;
    }

    public String getEmployerCountry() {
        return employerCountry;
    }

    public void setEmployerCountry(String employerCountry) {
        this.employerCountry = employerCountry;
    }

    public Boolean getEmployerCountryVerified() {
        return employerCountryVerified;
    }

    public void setEmployerCountryVerified(Boolean employerCountryVerified) {
        this.employerCountryVerified = employerCountryVerified;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public Boolean getJobTitleVerified() {
        return jobTitleVerified;
    }

    public void setJobTitleVerified(Boolean jobTitleVerified) {
        this.jobTitleVerified = jobTitleVerified;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Boolean getStartDateVerified() {
        return startDateVerified;
    }

    public void setStartDateVerified(Boolean startDateVerified) {
        this.startDateVerified = startDateVerified;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getEndDateVerified() {
        return endDateVerified;
    }

    public void setEndDateVerified(Boolean endDateVerified) {
        this.endDateVerified = endDateVerified;
    }

    public String getCurrentEmployment() {
        return currentEmployment;
    }

    public void setCurrentEmployment(String currentEmployment) {
        this.currentEmployment = currentEmployment;
    }

    public Boolean getCurrentEmploymentVerified() {
        return currentEmploymentVerified;
    }

    public void setCurrentEmploymentVerified(Boolean currentEmploymentVerified) {
        this.currentEmploymentVerified = currentEmploymentVerified;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public Boolean getContractTypeVerified() {
        return contractTypeVerified;
    }

    public void setContractTypeVerified(Boolean contractTypeVerified) {
        this.contractTypeVerified = contractTypeVerified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}