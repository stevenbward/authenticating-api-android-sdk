package hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification;

import com.google.gson.annotations.SerializedName;

public class License {
    @SerializedName("license_title")
    private String licenseTitle;
    @SerializedName("license_organization")
    private String licenseOrganization;
    @SerializedName("license_number")
    private String licenseNumber;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("state")
    private String state;
    @SerializedName("country")
    private String country;

    public License(String licenseTitle, String licenseOrganization, String licenseNumber, String startDate, String endDate, String state, String country) {
        this.licenseTitle = licenseTitle;
        this.licenseOrganization = licenseOrganization;
        this.licenseNumber = licenseNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        this.state = state;
        this.country = country;
    }

    public String getLicenseTitle() {
        return licenseTitle;
    }

    public void setLicenseTitle(String licenseTitle) {
        this.licenseTitle = licenseTitle;
    }

    public String getLicenseOrganization() {
        return licenseOrganization;
    }

    public void setLicenseOrganization(String licenseOrganization) {
        this.licenseOrganization = licenseOrganization;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
