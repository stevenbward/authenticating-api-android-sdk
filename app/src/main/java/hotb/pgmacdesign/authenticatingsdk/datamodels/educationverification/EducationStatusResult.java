package hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification;

import com.google.gson.annotations.SerializedName;

public class EducationStatusResult {

    @SerializedName("institution_name")
    private String institutionName;
    @SerializedName("institution_name_verified")
    private Boolean institutionNameVerified;
    @SerializedName("institution_type")
    private String institutionType;
    @SerializedName("institution_campus_name")
    private String institutionCampusName;
    @SerializedName("institution_phone")
    private String institutionPhone;
    @SerializedName("institution_address")
    private String institutionAddress;
    @SerializedName("institution_address_verified")
    private Boolean institutionAddressVerified;
    @SerializedName("institution_town")
    private String institutionTown;
    @SerializedName("institution_town_verified")
    private Boolean institutionTownVerified;
    @SerializedName("institution_country")
    private String institutionCountry;
    @SerializedName("institution_country_verified")
    private Boolean institutionCountryVerified;
    @SerializedName("institution_postal_code")
    private String institutionPostalCode;
    @SerializedName("degree")
    private String degree;
    @SerializedName("degree_verified")
    private Boolean degreeVerified;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("start_date_verified")
    private Boolean startDateVerified;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("end_date_verified")
    private Boolean endDateVerified;
    @SerializedName("majors")
    private String majors;
    @SerializedName("majors_verified")
    private Boolean majorsVerified;
    @SerializedName("minors")
    private String minors;
    @SerializedName("minors_verified")
    private Boolean minorsVerified;
    @SerializedName("honors")
    private String honors;
    @SerializedName("honors_verified")
    private Boolean honorsVerified;
    @SerializedName("currently_attending")
    private Integer currentlyAttending;
    @SerializedName("currently_attending_verified")
    private Boolean currentlyAttendingVerified;
    @SerializedName("completed_successfully")
    private Integer completedSuccessfully;
    @SerializedName("completed_successfully_verified")
    private Boolean completedSuccessfullyVerified;
    @SerializedName("status")
    private String status;

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public Boolean getInstitutionNameVerified() {
        return institutionNameVerified;
    }

    public void setInstitutionNameVerified(Boolean institutionNameVerified) {
        this.institutionNameVerified = institutionNameVerified;
    }

    public String getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(String institutionType) {
        this.institutionType = institutionType;
    }

    public String getInstitutionCampusName() {
        return institutionCampusName;
    }

    public void setInstitutionCampusName(String institutionCampusName) {
        this.institutionCampusName = institutionCampusName;
    }

    public String getInstitutionPhone() {
        return institutionPhone;
    }

    public void setInstitutionPhone(String institutionPhone) {
        this.institutionPhone = institutionPhone;
    }

    public String getInstitutionAddress() {
        return institutionAddress;
    }

    public void setInstitutionAddress(String institutionAddress) {
        this.institutionAddress = institutionAddress;
    }

    public Boolean getInstitutionAddressVerified() {
        return institutionAddressVerified;
    }

    public void setInstitutionAddressVerified(Boolean institutionAddressVerified) {
        this.institutionAddressVerified = institutionAddressVerified;
    }

    public String getInstitutionTown() {
        return institutionTown;
    }

    public void setInstitutionTown(String institutionTown) {
        this.institutionTown = institutionTown;
    }

    public Boolean getInstitutionTownVerified() {
        return institutionTownVerified;
    }

    public void setInstitutionTownVerified(Boolean institutionTownVerified) {
        this.institutionTownVerified = institutionTownVerified;
    }

    public String getInstitutionCountry() {
        return institutionCountry;
    }

    public void setInstitutionCountry(String institutionCountry) {
        this.institutionCountry = institutionCountry;
    }

    public Boolean getInstitutionCountryVerified() {
        return institutionCountryVerified;
    }

    public void setInstitutionCountryVerified(Boolean institutionCountryVerified) {
        this.institutionCountryVerified = institutionCountryVerified;
    }

    public String getInstitutionPostalCode() {
        return institutionPostalCode;
    }

    public void setInstitutionPostalCode(String institutionPostalCode) {
        this.institutionPostalCode = institutionPostalCode;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public Boolean getDegreeVerified() {
        return degreeVerified;
    }

    public void setDegreeVerified(Boolean degreeVerified) {
        this.degreeVerified = degreeVerified;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Boolean getStartDateVerified() {
        return startDateVerified;
    }

    public void setStartDateVerified(Boolean startDateVerified) {
        this.startDateVerified = startDateVerified;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getEndDateVerified() {
        return endDateVerified;
    }

    public void setEndDateVerified(Boolean endDateVerified) {
        this.endDateVerified = endDateVerified;
    }

    public String getMajors() {
        return majors;
    }

    public void setMajors(String majors) {
        this.majors = majors;
    }

    public Boolean getMajorsVerified() {
        return majorsVerified;
    }

    public void setMajorsVerified(Boolean majorsVerified) {
        this.majorsVerified = majorsVerified;
    }

    public String getMinors() {
        return minors;
    }

    public void setMinors(String minors) {
        this.minors = minors;
    }

    public Boolean getMinorsVerified() {
        return minorsVerified;
    }

    public void setMinorsVerified(Boolean minorsVerified) {
        this.minorsVerified = minorsVerified;
    }

    public String getHonors() {
        return honors;
    }

    public void setHonors(String honors) {
        this.honors = honors;
    }

    public Boolean getHonorsVerified() {
        return honorsVerified;
    }

    public void setHonorsVerified(Boolean honorsVerified) {
        this.honorsVerified = honorsVerified;
    }

    public Integer getCurrentlyAttending() {
        return currentlyAttending;
    }

    public void setCurrentlyAttending(Integer currentlyAttending) {
        this.currentlyAttending = currentlyAttending;
    }

    public Boolean getCurrentlyAttendingVerified() {
        return currentlyAttendingVerified;
    }

    public void setCurrentlyAttendingVerified(Boolean currentlyAttendingVerified) {
        this.currentlyAttendingVerified = currentlyAttendingVerified;
    }

    public Integer getCompletedSuccessfully() {
        return completedSuccessfully;
    }

    public void setCompletedSuccessfully(Integer completedSuccessfully) {
        this.completedSuccessfully = completedSuccessfully;
    }

    public Boolean getCompletedSuccessfullyVerified() {
        return completedSuccessfullyVerified;
    }

    public void setCompletedSuccessfullyVerified(Boolean completedSuccessfullyVerified) {
        this.completedSuccessfullyVerified = completedSuccessfullyVerified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
