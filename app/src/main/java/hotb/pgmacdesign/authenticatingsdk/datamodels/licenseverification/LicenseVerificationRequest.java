
package hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LicenseVerificationRequest {

    @SerializedName("userAccessCode")
    private String userAccessCode;
    @SerializedName("licenseList")
    private List<License> licenseList;

    public LicenseVerificationRequest(String userAccessCode, List<License> licenseList) {
        this.userAccessCode = userAccessCode;
        this.licenseList = licenseList;
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }

    public List<License> getLicenseList() {
        return licenseList;
    }

    public void setLicenseList(List<License> licenseList) {
        this.licenseList = licenseList;
    }
}
