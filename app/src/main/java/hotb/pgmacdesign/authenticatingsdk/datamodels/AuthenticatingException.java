package hotb.pgmacdesign.authenticatingsdk.datamodels;

import android.support.annotation.Nullable;

public class AuthenticatingException extends Exception {

    private String authErrorString;
    private String authErrorStringDetails;

    public AuthenticatingException(String authErrorString) {
        this(authErrorString, null);
    }

    public AuthenticatingException(Throwable t) {
        super(t);
        this.authErrorString = t.getMessage();
        this.authErrorStringDetails = t.getLocalizedMessage();
    }

    public AuthenticatingException(Throwable t, String authErrorString) {
        super(authErrorString, t);
        this.authErrorString = authErrorString;
        this.authErrorStringDetails = t.getLocalizedMessage();
    }

    public AuthenticatingException(String authErrorString, String authErrorStringDetails) {
        super(authErrorString);
        this.authErrorString = authErrorString;
        this.authErrorStringDetails = authErrorStringDetails;
    }

    @Nullable
    public String getAuthErrorString() {
        return authErrorString;
    }

    @Nullable
    public String getAuthErrorStringDetails() {
        return authErrorStringDetails;
    }
}
