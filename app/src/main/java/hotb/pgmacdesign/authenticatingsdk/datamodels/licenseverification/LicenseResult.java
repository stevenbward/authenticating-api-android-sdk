package hotb.pgmacdesign.authenticatingsdk.datamodels.licenseverification;

import com.google.gson.annotations.SerializedName;

import hotb.pgmacdesign.authenticatingsdk.datamodels.ErrorParsingObj;

public class LicenseResult extends ErrorParsingObj {
    @SerializedName("license_title")
    private String licenseTitle;
    @SerializedName("license_title_verified")
    private Boolean licenseTitleVerified;
    @SerializedName("license_organization")
    private String licenseOrganization;
    @SerializedName("license_organization_verified")
    private Boolean licenseOrganizationVerified;
    @SerializedName("license_number")
    private String licenseNumber;
    @SerializedName("license_number_verified")
    private Boolean licenseNumberVerified;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("start_date_verified")
    private Boolean startDateVerified;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("end_date_verified")
    private Boolean endDateVerified;
    @SerializedName("state")
    private String state;
    @SerializedName("country")
    private String country;
    @SerializedName("status")
    private String status;

    public String getLicenseTitle() {
        return licenseTitle;
    }

    public void setLicenseTitle(String licenseTitle) {
        this.licenseTitle = licenseTitle;
    }

    public Boolean getLicenseTitleVerified() {
        return licenseTitleVerified;
    }

    public void setLicenseTitleVerified(Boolean licenseTitleVerified) {
        this.licenseTitleVerified = licenseTitleVerified;
    }

    public String getLicenseOrganization() {
        return licenseOrganization;
    }

    public void setLicenseOrganization(String licenseOrganization) {
        this.licenseOrganization = licenseOrganization;
    }

    public Boolean getLicenseOrganizationVerified() {
        return licenseOrganizationVerified;
    }

    public void setLicenseOrganizationVerified(Boolean licenseOrganizationVerified) {
        this.licenseOrganizationVerified = licenseOrganizationVerified;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Boolean getLicenseNumberVerified() {
        return licenseNumberVerified;
    }

    public void setLicenseNumberVerified(Boolean licenseNumberVerified) {
        this.licenseNumberVerified = licenseNumberVerified;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Boolean getStartDateVerified() {
        return startDateVerified;
    }

    public void setStartDateVerified(Boolean startDateVerified) {
        this.startDateVerified = startDateVerified;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getEndDateVerified() {
        return endDateVerified;
    }

    public void setEndDateVerified(Boolean endDateVerified) {
        this.endDateVerified = endDateVerified;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
