
package hotb.pgmacdesign.authenticatingsdk.datamodels.quiz;

import com.google.gson.annotations.SerializedName;

public class KBAStatus {

    @SerializedName("QuestionsAnswered")
    private Integer questionsAnswered;
    @SerializedName("NoOfQuestions")
    private Integer noOfQuestions;

    public Integer getQuestionsAnswered() {
        return questionsAnswered;
    }

    public void setQuestionsAnswered(Integer questionsAnswered) {
        this.questionsAnswered = questionsAnswered;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }
}
