
package hotb.pgmacdesign.authenticatingsdk.datamodels.verifyquiz;

import com.google.gson.annotations.SerializedName;

public class QuestionAnswer {

    @SerializedName("QuestionId")
    private Integer questionId;
    @SerializedName("AnswerId")
    private Integer answerId;

    public QuestionAnswer(Integer questionId, Integer answerId) {
        this.questionId = questionId;
        this.answerId = answerId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }
}
