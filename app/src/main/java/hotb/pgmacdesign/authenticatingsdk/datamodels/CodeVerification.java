package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

import hotb.pgmacdesign.authenticatingsdk.networking.AuthenticatingAPICalls;

/**
 * This class is used for phone verification
 * {@link AuthenticatingAPICalls}
 * Created by pmacdowell on 2017-07-24.
 */

public class CodeVerification extends ErrorParsingObj {
    @SerializedName("userAccessCode")
    private String userAccessCode;
    @SerializedName("code")
    private String smsCode;

    public CodeVerification(String userAccessCode, String smsCode) {
        this.userAccessCode = userAccessCode;
        this.smsCode = smsCode;
    }

    public String getAccessCode() {
        return userAccessCode;
    }

    public void setAccessCode(String accessCode) {
        this.userAccessCode = accessCode;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }
}
