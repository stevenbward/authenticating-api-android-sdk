
package hotb.pgmacdesign.authenticatingsdk.datamodels.employmentverification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmploymentDetail {

    @SerializedName("employerName")
    @Expose
    private String employerName;
    @SerializedName("employerPhone")
    @Expose
    private String employerPhone;
    @SerializedName("employerAddress")
    @Expose
    private String employerAddress;
    @SerializedName("employerAddressTown")
    @Expose
    private String employerAddressTown;
    @SerializedName("employerAddressCountry")
    @Expose
    private String employerAddressCountry;
    @SerializedName("employerAddressStateProvince")
    @Expose
    private String employerAddressStateProvince;
    @SerializedName("employerAddressPostalCode")
    @Expose
    private String employerAddressPostalCode;
    @SerializedName("jobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("dateEmployedFrom")
    @Expose
    private String dateEmployedFrom;
    @SerializedName("dateEmployedTo")
    @Expose
    private String dateEmployedTo;
    @SerializedName("supervisorName")
    @Expose
    private String supervisorName;
    @SerializedName("supervisorContactInfo")
    @Expose
    private String supervisorContactInfo;
    @SerializedName("currentEmployment")
    @Expose
    private String currentEmployment;
    @SerializedName("contractType")
    @Expose
    private String contractType;

    public EmploymentDetail(String employerName, String employerPhone, String employerAddress, String employerAddressTown, String employerAddressCountry, String employerAddressStateProvince, String employerAddressPostalCode, String jobTitle, String dateEmployedFrom, String dateEmployedTo, String supervisorName, String supervisorContactInfo, String currentEmployment, String contractType) {
        this.employerName = employerName;
        this.employerPhone = employerPhone;
        this.employerAddress = employerAddress;
        this.employerAddressTown = employerAddressTown;
        this.employerAddressCountry = employerAddressCountry;
        this.employerAddressStateProvince = employerAddressStateProvince;
        this.employerAddressPostalCode = employerAddressPostalCode;
        this.jobTitle = jobTitle;
        this.dateEmployedFrom = dateEmployedFrom;
        this.dateEmployedTo = dateEmployedTo;
        this.supervisorName = supervisorName;
        this.supervisorContactInfo = supervisorContactInfo;
        this.currentEmployment = currentEmployment;
        this.contractType = contractType;
    }

    public String getEmployerName() {
        return employerName;
    }

    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    public String getEmployerPhone() {
        return employerPhone;
    }

    public void setEmployerPhone(String employerPhone) {
        this.employerPhone = employerPhone;
    }

    public String getEmployerAddress() {
        return employerAddress;
    }

    public void setEmployerAddress(String employerAddress) {
        this.employerAddress = employerAddress;
    }

    public String getEmployerAddressTown() {
        return employerAddressTown;
    }

    public void setEmployerAddressTown(String employerAddressTown) {
        this.employerAddressTown = employerAddressTown;
    }

    public String getEmployerAddressCountry() {
        return employerAddressCountry;
    }

    public void setEmployerAddressCountry(String employerAddressCountry) {
        this.employerAddressCountry = employerAddressCountry;
    }

    public String getEmployerAddressStateProvince() {
        return employerAddressStateProvince;
    }

    public void setEmployerAddressStateProvince(String employerAddressStateProvince) {
        this.employerAddressStateProvince = employerAddressStateProvince;
    }

    public String getEmployerAddressPostalCode() {
        return employerAddressPostalCode;
    }

    public void setEmployerAddressPostalCode(String employerAddressPostalCode) {
        this.employerAddressPostalCode = employerAddressPostalCode;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDateEmployedFrom() {
        return dateEmployedFrom;
    }

    public void setDateEmployedFrom(String dateEmployedFrom) {
        this.dateEmployedFrom = dateEmployedFrom;
    }

    public String getDateEmployedTo() {
        return dateEmployedTo;
    }

    public void setDateEmployedTo(String dateEmployedTo) {
        this.dateEmployedTo = dateEmployedTo;
    }

    public String getSupervisorName() {
        return supervisorName;
    }

    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    public String getSupervisorContactInfo() {
        return supervisorContactInfo;
    }

    public void setSupervisorContactInfo(String supervisorContactInfo) {
        this.supervisorContactInfo = supervisorContactInfo;
    }

    public String getCurrentEmployment() {
        return currentEmployment;
    }

    public void setCurrentEmployment(String currentEmployment) {
        this.currentEmployment = currentEmployment;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

}
