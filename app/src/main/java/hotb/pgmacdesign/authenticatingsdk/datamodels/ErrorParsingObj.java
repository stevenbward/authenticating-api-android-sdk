package hotb.pgmacdesign.authenticatingsdk.datamodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Error information class.
 * Created by pmacdowell on 2017-07-13.
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ErrorParsingObj {

    @SerializedName("missingInfo")
    private List<String>missingInfo;
    @SerializedName("errorMessage")
    private String errorMessage;

    public List<String> getMissingInfo() {
        return missingInfo;
    }

    public void setMissingInfo(List<String> missingInfo) {
        this.missingInfo = missingInfo;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
