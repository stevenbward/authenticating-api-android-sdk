package hotb.pgmacdesign.authenticatingsdk.datamodels.educationverification;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import hotb.pgmacdesign.authenticatingsdk.datamodels.ErrorParsingObj;

public class EducationVerificationRequest extends ErrorParsingObj {

    @SerializedName("userAccessCode")
    private String userAccessCode;
    @SerializedName("educationList")
    private List<Education> education;

    public EducationVerificationRequest(String userAccessCode, List<Education> education) {
        this.userAccessCode = userAccessCode;
        this.education = education;
    }

    public String getUserAccessCode() {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode) {
        this.userAccessCode = userAccessCode;
    }

    public List<Education> getEducation() {
        return education;
    }

    public void setEducation(List<Education> education) {
        this.education = education;
    }
}
