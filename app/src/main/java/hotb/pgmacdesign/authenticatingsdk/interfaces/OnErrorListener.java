package hotb.pgmacdesign.authenticatingsdk.interfaces;

import android.support.annotation.NonNull;

import hotb.pgmacdesign.authenticatingsdk.datamodels.AuthenticatingException;

public interface OnErrorListener {
    /**
     * @param authenticatingException returns an authenticate error on failure
     */
    void onError(@NonNull AuthenticatingException authenticatingException);
}
