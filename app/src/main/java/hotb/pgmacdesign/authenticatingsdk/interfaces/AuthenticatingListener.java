package hotb.pgmacdesign.authenticatingsdk.interfaces;

import android.support.annotation.NonNull;

/**
 * @param <T> will be returned in onSuccess if the api call is successfull
 */
public interface AuthenticatingListener<T> extends OnErrorListener {
    void onSuccess(@NonNull T t);
}
